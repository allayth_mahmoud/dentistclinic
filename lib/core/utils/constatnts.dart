import 'package:flutter/material.dart';

const String baseUrl =
    'https://dentistclinic-fcd69-default-rtdb.firebaseio.com';
const myMainColor = Color.fromARGB(255, 100, 94, 94);
const mySecondColor = Color.fromARGB(255, 47, 143, 221);
const myErrorColor = Colors.red;
