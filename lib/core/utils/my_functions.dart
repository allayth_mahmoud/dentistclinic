
List<Map<String, dynamic>> transform(Map<String, dynamic> input) {
  List<Map<String, dynamic>> output = [];

  input.forEach((key, value) {
    value['id'] = key ;
    output.add(value);
  });

  return output;
}
