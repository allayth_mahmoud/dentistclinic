import 'package:flutter/material.dart';
// ignore: depend_on_referenced_packages
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

import 'animations/size_transition.dart';
import 'core/utils/constatnts.dart';
import 'core/widgets/wave_clipper.dart';
import 'features/patients/presentation/screens/add_patient_screen.dart';
import 'features/patients/presentation/screens/patients_list_screen.dart';
import 'features/sessions/presentation/screens/schedule_screen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  static const routeName = '/home-screen';

  @override
  Widget build(BuildContext context) {
    var w = MediaQuery.sizeOf(context).width;
    int columnCount = 2;
    final media = MediaQuery.sizeOf(context);
    return Scaffold(
      backgroundColor: Colors.white.withOpacity(0.97),
      body: Column(
        children: [
          // AppBar(
          //   backgroundColor: myMainColor,
          // ),
          Stack(
            children: [
              Opacity(
                opacity: 0.3,
                child: ClipPath(
                  clipper: WaveClipper(),
                  child: Container(
                    height: media.height * 0.32,
                    color: myMainColor,
                  ),
                ),
              ),
              ClipPath(
                clipper: WaveClipper(),
                child: Container(
                  color: myMainColor,
                  height: media.height * 0.31,
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          width: media.width * 0.5,
                          height: media.height * 0.3,
                          child: Image.asset(
                            'assets/images/logo.png',
                            fit: BoxFit.contain,
                          ),
                        ),
                        Text(
                          'Dentist Clinic',
                          style: TextStyle(
                            fontSize: media.width * 0.07,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: media.height * 0.06,
          ),
          Expanded(
              child: AnimationLimiter(
            child: GridView.count(
              childAspectRatio: 3 / 1,
              crossAxisSpacing: 20,
              mainAxisSpacing: 25,
              physics: const BouncingScrollPhysics(
                  // parent: AlwaysScrollableScrollPhysics()
                  ),
              padding: EdgeInsets.all(w / 38),
              crossAxisCount: 1,
              children: [
                AnimatedGridItem(
                    position: 1,
                    columnCount: 1,
                    child: OptionItem(
                      title: 'View Patients',
                      onTap: () {
                        Navigator.push(
                                context, SizeTransition1(PationtsListScreen()))
                            // .then((value) =>
                            //     )
                            ;
                        // // Navigator.of(context)
                        // //     .pushNamed(AllPatientsScreen.routeName);
                      },
                      icon: const Icon(
                        Icons.groups,
                        size: 50,
                        color: Colors.blueGrey,
                      ),
                    )),
                AnimatedGridItem(
                  position: 3,
                  columnCount: columnCount,
                  child: OptionItem(
                    title: 'Schedule',
                    onTap: () {
                      Navigator.push(
                          context, SizeTransition1(ScheduleScreen()));
                      // // Navigator.of(context).pushNamed(ScheduleScreen.routeName);
                    },
                    icon: const Icon(
                      Icons.calendar_month_outlined,
                      size: 50,
                      color: Colors.blueGrey,
                    ),
                  ),
                ),
                AnimatedGridItem(
                  position: 2,
                  columnCount: columnCount,
                  child: OptionItem(
                    title: 'Add Patient',
                    onTap: () {
                      Navigator.push(
                          context,
                          SizeTransition1(AddPatientScreen(
                            edit: false,
                          )));
                      // /* Navigator.of(context).pushNamed(StepperScreen.routeName); */
                    },
                    icon: const Icon(
                      Icons.person_add,
                      size: 50,
                      color: Colors.blueGrey,
                    ),
                  ),
                ),
                // AnimatedGridItem(
                //   position: 3,
                //   columnCount: columnCount,
                //   child: OptionItem(
                //     title: 'Settings',
                //     onTap: () {
                //       // Navigator.push(context,
                //       //     SizeTransition1(const SettingsScreen()));
                //       // // Navigator.of(context).pushNamed(ScheduleScreen.routeName);
                //     },
                //     icon: const Icon(
                //       Icons.settings,
                //       size: 50,
                //       color: Colors.blueGrey,
                //     ),
                //   ),
                // )
              ],
            ),
          )),
        ],
      ),
    );
  }
}

class AnimatedGridItem extends StatelessWidget {
  AnimatedGridItem({
    Key? key,
    required this.columnCount,
    required this.child,
    required this.position,
  }) : super(key: key);

  final int columnCount;
  final int position;
  final OptionItem child;

  @override
  Widget build(BuildContext context) {
    return AnimationConfiguration.staggeredGrid(
      position: position,
      duration: const Duration(milliseconds: 500),
      columnCount: columnCount,
      child: ScaleAnimation(
        duration: const Duration(milliseconds: 900),
        curve: Curves.fastLinearToSlowEaseIn,
        child: FadeInAnimation(child: child),
      ),
    );
  }
}

//import 'package:flutter/material.dart';

class OptionItem extends StatelessWidget {
  final String title;
  final Icon icon;

  Function onTap;

  OptionItem({
    required this.title,
    required this.icon,
    required this.onTap,
  });

  void onSelected() {}

  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.sizeOf(context);
    return InkWell(
      onTap: () {
        onTap();
      },
      child: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.all(20),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: const Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            icon,
            SizedBox(
              width: media.width * 0.2,
            ),
            Text(
              title,
              style: const TextStyle(
                  color: Colors.blue,
                  fontFamily: 'Lato',
                  fontWeight: FontWeight.bold,
                  fontSize: 24),
            ),
          ],
        ),
      ),
    );
  }
}
