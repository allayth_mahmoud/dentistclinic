import '../../domain/entities/session.dart';

class SessionModel extends Session {
  final String patientId;

  final String? sessionId;

  final String? sessionDate;
  final String? notices;

  final String? description;
  final int status;

  SessionModel(
      {required this.sessionId,
      required this.patientId,
      required this.sessionDate,
      required this.status,
      this.notices,
      this.description})
      : super(sessionId: '', patientId: '', sessionDate: null, status: 0);

  factory SessionModel.fromMap(Map<String, dynamic> json) {
    return SessionModel(
        sessionId: json['id'].toString(),
        patientId: json['patient_id'].toString(),
        sessionDate: json['session_date'].toString(),
        status: json['status'],
        notices: json['notices'],
        description: json['description']);
  }

  Map<String, dynamic> toJson() {
    return {
      "id": sessionId,
      "patient_id": patientId,
      "session_date": sessionDate,
      "status": status,
      "notices": notices,
      "description": description,
    };
  }
}
