import 'package:dartz/dartz.dart';

import '../../../../core/error/exception.dart';
import '../../../../core/error/failures.dart';
import '../../../../core/network/network_info.dart';
import '../../domain/entities/session.dart';
import '../../domain/repositories/session_repositories.dart';
import '../datasources/session_local_data_source.dart';
import '../datasources/session_remote_data_source.dart';
import '../models/session_model.dart';

class SessionRepositoryImpl implements SessionRepository {
  final SessionRemoteDataSource remoteDataSource;
  final SessionLocalDataSource localDataSource;
  final NetworkInfo networkInfo;

  SessionRepositoryImpl(
      this.remoteDataSource, this.localDataSource, this.networkInfo);

  @override
  Future<Either<Failure, List<Session>>> getAllSessionsByDate(
      String date) async {
    if (await networkInfo.isConnected) {
      try {
            print('get date repo');

        final sessions = await remoteDataSource.getAllSessionByDate(date);
        localDataSource.cachSessions(sessions);
        return Right(sessions.cast<Session>());
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(OfflineFailure());
    }
  }

  @override
  Future<Either<Failure, List<Session>>> getAllSessionsByPatientId(
      String patientId) async {
    if (await networkInfo.isConnected) {
      try {
        final sessions =
            await remoteDataSource.getAllSessionByPatientId(patientId);
        localDataSource.cachSessions(sessions);
        return Right(sessions);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      // try {
      //   final localPatients = await localDataSource.getCachedPatients();
      //   return Right(localPatients.cast<Patient>());
      // } on EmptyCacheException {
      //   return Left(EmptyCacheFailure());
      // }

      return Left(OfflineFailure());
    }
  }

  @override
  Future<Either<Failure, Unit>> addSession(Session session) async {
    print('addsession repo');
    final sessionModel = SessionModel(
        sessionId: session.sessionId,
        patientId: session.patientId,
        sessionDate: session.sessionDate,
        status: session.status,
        notices: session.notices,
        description: session.description);
    return await _getMessage(() {
      return remoteDataSource.addSession(sessionModel);
    });
  }

  @override
  Future<Either<Failure, Unit>> updateSession(Session session) async {
    final sessionModel = SessionModel(
        sessionId: session.sessionId,
        patientId: session.patientId,
        sessionDate: session.sessionDate,
        status: session.status,
        notices: session.notices,
        description: session.description);
    print('update session repo');

    return await _getMessage(() {
      return remoteDataSource.updateSession(sessionModel);
    });
  }

  @override
  Future<Either<Failure, Unit>> deleteSession(String sessionId) async {
    return await _getMessage(() => remoteDataSource.deleteSession(sessionId));
  }

  Future<Either<Failure, Unit>> _getMessage(
      Future<Unit> Function() deleteOrUpdateOrAddPatient) async {
    if (await networkInfo.isConnected) {
      try {
        await deleteOrUpdateOrAddPatient();
        return Right(unit);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(OfflineFailure());
    }
  }
}
