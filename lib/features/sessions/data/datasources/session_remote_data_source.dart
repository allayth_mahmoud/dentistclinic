import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:http/http.dart' as http;

import '../../../../core/error/exception.dart';
import '../../../../core/utils/constatnts.dart';
import '../../../../core/utils/my_functions.dart';
import '../models/session_model.dart';

abstract class SessionRemoteDataSource {
  Future<List<SessionModel>> getAllSessionByDate(String date);
  Future<List<SessionModel>> getAllSessionByPatientId(String patientId);
  Future<Unit> deleteSession(String sessionId);
  Future<Unit> updateSession(SessionModel session);
  Future<Unit> addSession(SessionModel session);
}

const base_url = '';

class SessionRemoteDataSourceImpl implements SessionRemoteDataSource {
  late final http.Client client;
  SessionRemoteDataSourceImpl({required this.client});

  @override
  Future<List<SessionModel>> getAllSessionByDate(String date) async {
    print('1fffffffffffffffffff');
    print(date);
    final response = await client.get(Uri.parse(
        'https://dentistclinic-fcd69-default-rtdb.firebaseio.com/session.json'));
    var responseBody = transform(json.decode(response.body));
    print('2fffffffffffffffffff');

    if (response.statusCode == 200) {
      final List decodedJson = responseBody;
      List<SessionModel> sessionsList = decodedJson
          .map((sessionModel) => SessionModel.fromMap(sessionModel))
          .where(
              (element) => element.sessionDate!.toLowerCase().startsWith(date))
          .toList();
      return sessionsList;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<List<SessionModel>> getAllSessionByPatientId(String patientId) async {
    final response = await client.get(Uri.parse(
        'https://dentistclinic-fcd69-default-rtdb.firebaseio.com/session.json'));
    var responseBody = transform(json.decode(response.body));
    if (response.statusCode == 200) {
      final List decodedJson = responseBody;
      List<SessionModel> sessionsList = decodedJson
          .map((sessionModel) => SessionModel.fromMap(sessionModel))
          .where((element) => element.patientId == patientId)
          .toList();
      return sessionsList;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<Unit> addSession(SessionModel session) async {
    print('1ffffffffffffff');
    final body = {
      //"id": session.sessionId,
      "patient_id": session.patientId,
      "session_date": session.sessionDate,
      "status": session.status,
      "notices": session.notices,
      "description": session.description,
    };
    final response = await client.post(Uri.parse(baseUrl + '/session.json'),
        body: json.encode(body));
    print('2ffffffffffffff');

    if (response.statusCode == 200) {
      return Future.value(unit);
    } else {
      throw ServerException();
    }
  }

  @override
  Future<Unit> deleteSession(String sessionId) async {
    final response = await client.delete(Uri.parse(base_url));
    if (response.statusCode == 200) {
      return Future.value(unit);
    } else {
      throw ServerException();
    }
  }

  @override
  Future<Unit> updateSession(SessionModel session) async {
    final body = {
      //  "id": session.sessionId,
      "patient_id": session.patientId,
      "session_date": session.sessionDate,
      "status": session.status,
      "notices": session.notices,
      "description": session.description,
    };
    print(body);
    print(session.sessionId);
    print('1fffffffffffffffff');

    final response = await client.put(
        Uri.parse(baseUrl + '/session/${session.sessionId}.json'),
        body: json.encode(body));
    print('2fffffffffffffffff');

    if (response.statusCode == 200) {
      return Future.value(unit);
    } else {
      throw ServerException();
    }
  }
}
