import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../core/error/exception.dart';
import '../models/session_model.dart';

abstract class SessionLocalDataSource {
  Future<List<SessionModel>> getCachedSessions();
  Future<Unit> cachSessions(List<SessionModel> sessionsModel);
}

const cached_sessions = 'CACHED_SESSIONS';

class SessionLocalDataSourceImpl implements SessionLocalDataSource {
  final SharedPreferences sharedPreferences;
  SessionLocalDataSourceImpl({required this.sharedPreferences});
  @override
  Future<Unit> cachSessions(List<SessionModel> sessionsModel) {
    final sessionModelToJson =
        sessionsModel.map((sessionModel) => sessionModel.toJson()).toList();

    sharedPreferences.setString(
        cached_sessions, json.encode(sessionModelToJson));
    return Future.value(unit);
  }

  @override
  Future<List<SessionModel>> getCachedSessions() async {
    final sessionString = sharedPreferences.getString(cached_sessions);
    if (sessionString != null) {
      final List sessionJson = json.decode(sessionString);
      final List<SessionModel> sessionJsonToModel = sessionJson
          .map((sessionJson) => SessionModel.fromMap(sessionJson))
          .toList();
      return sessionJsonToModel;
    } else {
      throw EmptyCacheException();
    }
  }
}
