import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../entities/session.dart';
import '../repositories/session_repositories.dart';

class GetSessionByDateUseCase {
  final SessionRepository repository;
  GetSessionByDateUseCase({required this.repository});

  Future<Either<Failure, List<Session>>> call(String date) async {
    print('get date usecase');
    return await repository.getAllSessionsByDate(date);
  }
}
