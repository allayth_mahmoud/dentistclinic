import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../repositories/session_repositories.dart';
 
class DeleteSessionUseCase {
  final SessionRepository repository;
  DeleteSessionUseCase({required this.repository});

  Future<Either<Failure, Unit>> call(String sessionId) async {
    return await repository.deleteSession(sessionId);
  }
}
