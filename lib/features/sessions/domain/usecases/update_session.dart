import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../entities/session.dart';
import '../repositories/session_repositories.dart';

class UpdateSessionUseCase {
  final SessionRepository repository;
  UpdateSessionUseCase({required this.repository});

  Future<Either<Failure, Unit>> call(Session session) async {
    print('update session usecase');
    return await repository.updateSession(session);
  }
}
