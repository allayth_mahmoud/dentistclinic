import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../entities/session.dart';
import '../repositories/session_repositories.dart';

class GetSessionByPatientUseCase {
  final SessionRepository repository;
  GetSessionByPatientUseCase({required this.repository});

  Future<Either<Failure, List<Session>>> call(String patientId) async {
    return await repository.getAllSessionsByPatientId(patientId);
  }
}
