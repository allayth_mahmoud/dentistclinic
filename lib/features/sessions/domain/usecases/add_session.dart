import 'package:dartz/dartz.dart';
import '../repositories/session_repositories.dart';

import '../../../../core/error/failures.dart';
import '../entities/session.dart';

class AddSessionUseCase {
  final SessionRepository repository;
  AddSessionUseCase({required this.repository});

  Future<Either<Failure, Unit>> call(Session session) async {
    print('add session usecase');

    return await repository.addSession(session);
  }
}
