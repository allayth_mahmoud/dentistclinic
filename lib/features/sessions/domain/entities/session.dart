import 'package:equatable/equatable.dart';

class Session extends Equatable {
  final String patientId;

  final String? sessionId;

  final String? sessionDate;
  final String? notices;

  final String? description;
  final int status;

   Session(
      {required this.sessionId,
      required this.patientId,
      required this.sessionDate,
      required this.status,
      this.notices,
      this.description});

  @override
  List<Object?> get props => [sessionId, patientId, sessionDate, status];
}
