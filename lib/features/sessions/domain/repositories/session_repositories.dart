import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../entities/session.dart';

abstract class SessionRepository {
  Future<Either<Failure, List<Session>>> getAllSessionsByDate(String date);
  Future<Either<Failure, List<Session>>> getAllSessionsByPatientId(String patientId);
  Future<Either<Failure, Unit>> addSession(Session session);
  Future<Either<Failure, Unit>> updateSession(Session session);
  Future<Either<Failure, Unit>> deleteSession(String sessionId);
}
