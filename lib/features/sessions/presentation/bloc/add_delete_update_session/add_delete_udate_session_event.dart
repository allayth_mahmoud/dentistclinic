part of 'add_delete_udate_session_bloc.dart';

abstract class AddDeleteUdateSessionEvent extends Equatable {
  const AddDeleteUdateSessionEvent();

  @override
  List<Object> get props => [];
}

class AddSessionEvent extends AddDeleteUdateSessionEvent {
  final Session session;

  AddSessionEvent({required this.session});

    @override
  List<Object> get props => [session];
}

class UpdateSessionEvent extends AddDeleteUdateSessionEvent {
  final Session session;

  UpdateSessionEvent({required this.session});

    @override
  List<Object> get props => [session];
}

class DeleteSessionEvent extends AddDeleteUdateSessionEvent {
  final String sessionId;

  DeleteSessionEvent({required this.sessionId});

    @override
  List<Object> get props => [sessionId];
}


