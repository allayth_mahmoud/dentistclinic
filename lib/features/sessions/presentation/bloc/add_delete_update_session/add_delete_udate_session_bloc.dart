import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import '../../../../../core/strings/messages.dart';
import '../../../domain/usecases/add_session.dart';
import '../../../domain/usecases/delete_session.dart';
import '../../../domain/usecases/update_session.dart';
import 'package:equatable/equatable.dart';

import '../../../../../core/error/failures.dart';
import '../../../../../core/strings/failures.dart';
import '../../../domain/entities/session.dart';
 
part 'add_delete_udate_session_event.dart';
part 'add_delete_udate_session_state.dart';

class AddDeleteUdateSessionBloc
    extends Bloc<AddDeleteUdateSessionEvent, AddDeleteUdateSessionState> {
  final AddSessionUseCase addSessionUseCase;
  final UpdateSessionUseCase updateSessionUseCase;
  final DeleteSessionUseCase deleteSessionUseCase;

  AddDeleteUdateSessionBloc({
    required this.addSessionUseCase,
    required this.deleteSessionUseCase,
    required this.updateSessionUseCase,
  }) : super(AddDeleteUdateSessionInitial()) {
    on<AddDeleteUdateSessionEvent>((event, emit) async {
      if (event is AddSessionEvent) {
        emit(LoadingAddDeleteUdateSessionState());
        final failureOrDoneMessage = await addSessionUseCase(event.session);
        emit(_eitherDoneMessageOrErrorState(
            failureOrDoneMessage, ADD_SUCCESS_MESSAGE));
      }else if (event is UpdateSessionEvent) {
        emit(LoadingAddDeleteUdateSessionState());
        final failureOrDoneMessage = await updateSessionUseCase(event.session);
        emit(_eitherDoneMessageOrErrorState(
            failureOrDoneMessage, UPDATE_SUCCESS_MESSAGE));
      }else if (event is DeleteSessionEvent) {
        emit(LoadingAddDeleteUdateSessionState());
        final failureOrDoneMessage = await deleteSessionUseCase(event.sessionId);
        emit(_eitherDoneMessageOrErrorState(
            failureOrDoneMessage, DELETE_SUCCESS_MESSAGE));
      }
    });
  }

  AddDeleteUdateSessionState _eitherDoneMessageOrErrorState(
      Either<Failure, Unit> either, String message) {
    return either.fold(
      (failure) => ErrorAddDeleteUdateSessionState(
        message: _mapFailureToMessage(failure),
      ),
      (_) => MessageAddDeleteUdateSessionState(message: message),
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case OfflineFailure:
        return OFFLINE_FAILURE_MESSAGE;
      default:
        return "Unexpected Error , Please try again later .";
    }
  }
}
