part of 'add_delete_udate_session_bloc.dart';

abstract class AddDeleteUdateSessionState extends Equatable {
  const AddDeleteUdateSessionState();

  @override
  List<Object> get props => [];
}

class AddDeleteUdateSessionInitial extends AddDeleteUdateSessionState {}

class LoadingAddDeleteUdateSessionState extends AddDeleteUdateSessionState {}

class MessageAddDeleteUdateSessionState extends AddDeleteUdateSessionState {
  final String message;

  MessageAddDeleteUdateSessionState({required this.message});

  @override
  List<Object> get props => [message];
}
class ErrorAddDeleteUdateSessionState extends AddDeleteUdateSessionState {
  final String message;

  ErrorAddDeleteUdateSessionState({required this.message});

  @override
  List<Object> get props => [message];
}
