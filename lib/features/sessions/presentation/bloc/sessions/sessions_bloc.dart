import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../../../../core/error/failures.dart';
import '../../../../../core/strings/failures.dart';
import '../../../domain/entities/session.dart';
import '../../../domain/usecases/get_session_by_date.dart';
import '../../../domain/usecases/get_session_by_patient.dart';

part 'sessions_event.dart';
part 'sessions_state.dart';

class SessionsBloc extends Bloc<SessionsEvent, SessionsState> {
  final GetSessionByDateUseCase getSessionByDateUseCase;
  final GetSessionByPatientUseCase getSessionByPatientUseCase;
  SessionsBloc(this.getSessionByDateUseCase, this.getSessionByPatientUseCase)
      : super(SessionsInitial()) {
    on<SessionsEvent>((event, emit) async {
      if (event is GetAllSessionsByDateEvent) {
        emit(LoadingSessionsState());
        final failureOrSessions = await getSessionByDateUseCase(event.date);
        emit(_mapFailureOrPatientsToState(failureOrSessions));
      } else if (event is RefreshAllSessionsByDateEvent) {
        emit(LoadingSessionsState());
        final failureOrSessions = await getSessionByDateUseCase(event.date);
        emit(_mapFailureOrPatientsToState(failureOrSessions));
      } else if (event is GetAllSessionsByPatientEvent) {
        emit(LoadingSessionsState());
        print('111111');
        final failureOrSessions =
            await getSessionByPatientUseCase(event.patientId);
        print('22222');
        print(failureOrSessions);
        emit(_mapFailureOrPatientsToState(failureOrSessions));
        print('33333');
      } else if (event is RefreshAllSessionsByPatientEvent) {
        emit(LoadingSessionsState());
        final failureOrSessions =
            await getSessionByPatientUseCase(event.patientId);
        emit(_mapFailureOrPatientsToState(failureOrSessions));
      }
    });
  }

  SessionsState _mapFailureOrPatientsToState(
      Either<Failure, List<Session>> either) {
    return either.fold(
      (failure) => ErrorSessionsState(message: _mapFailureToMessage(failure)),
      (sessions) => LoadedSessionsState(
        sessions: sessions,
      ),
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case EmptyCacheFailure:
        return EMPTY_CACHE_FAILURE_MESSAGE;
      case OfflineFailure:
        return OFFLINE_FAILURE_MESSAGE;
      default:
        return "Unexpected Error , Please try again later .";
    }
  }
}
