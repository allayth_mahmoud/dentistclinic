part of 'sessions_bloc.dart';

abstract class SessionsEvent extends Equatable {
  const SessionsEvent();

  @override
  List<Object> get props => [];
}

class GetAllSessionsByDateEvent extends SessionsEvent {
  final String date;
  GetAllSessionsByDateEvent({required this.date});

  @override
  List<Object> get props => [date];
}

class RefreshAllSessionsByDateEvent extends SessionsEvent {
  final String date;
  RefreshAllSessionsByDateEvent({required this.date});

  @override
  List<Object> get props => [date];
}

class RefreshAllSessionsByPatientEvent extends SessionsEvent {
  final String patientId;
  RefreshAllSessionsByPatientEvent({required this.patientId});

  @override
  List<Object> get props => [patientId];
}

class GetAllSessionsByPatientEvent extends SessionsEvent {
  final String patientId;
  GetAllSessionsByPatientEvent({required this.patientId});

  @override
  List<Object> get props => [patientId];
}
