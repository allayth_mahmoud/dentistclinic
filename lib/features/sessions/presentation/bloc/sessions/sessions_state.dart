part of 'sessions_bloc.dart';

abstract class SessionsState extends Equatable {
  const SessionsState();

  @override
  List<Object> get props => [];
}

class SessionsInitial extends SessionsState {}

class LoadingSessionsState extends SessionsState {}

class LoadedSessionsState extends SessionsState {
  final List<Session> sessions;
  LoadedSessionsState({required this.sessions});
   @override
  List<Object> get props => [sessions];
}

class ErrorSessionsState extends SessionsState {
  final String message;
  ErrorSessionsState({required this.message});
   @override
  List<Object> get props => [message];
}