import 'package:date_picker_timeline/date_picker_timeline.dart';
import '../bloc/sessions/sessions_bloc.dart';
import '../widgets/session_list_for_date_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import '../../../../core/utils/constatnts.dart';

import '../../../../core/widgets/shimmer_widgets.dart';
import '../../../../core/widgets/loading_widget copy.dart';
import '../../../../core/widgets/message_display_widget.dart';

class ScheduleScreen extends StatefulWidget {
  const ScheduleScreen({Key? key}) : super(key: key);
  static const routeName = '/schedule_screen';

  @override
  State<ScheduleScreen> createState() => _ScheduleScreenState();
}

late DateTime _selectedDate;

List scheduleList = [
  {
    'profile_image': ';lj;lkj',
    'name': 'jack stinson',
    'session_date': '2020/2/2'
  },
  {
    'profile_image': ';lj;lkj',
    'name': 'jack stinson',
    'session_date': '2020/2/2'
  },
  {
    'profile_image': ';lj;lkj',
    'name': 'jack stinson',
    'session_date': '2020/2/2'
  },
  {
    'profile_image': ';lj;lkj',
    'name': 'jack stinson',
    'session_date': '2020/2/2'
  },
  {
    'profile_image': ';lj;lkj',
    'name': 'jack stinson',
    'session_date': '2020/2/2'
  },
  {
    'profile_image': ';lj;lkj',
    'name': 'jack stinson',
    'session_date': '2020/2/2'
  },
  {
    'profile_image': ';lj;lkj',
    'name': 'jack stinson',
    'session_date': '2020/2/2'
  },
];

class _ScheduleScreenState extends State<ScheduleScreen> {
  @override
  void initState() {
    _selectedDate = DateTime.now();

    super.initState();
  }

  bool isStatementExecuted = false;
  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.sizeOf(context);

    String date = DateFormat('yyyy-MM-dd').format(_selectedDate);

    BlocProvider.of<SessionsBloc>(context)
        .add(GetAllSessionsByDateEvent(date: date));

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: const Text(
          'Schedule',
        ),
        centerTitle: true,
        backgroundColor: myMainColor,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          // Date Picker
          Container(
            margin: const EdgeInsets.all(5),
            child: DatePicker(
              DateTime.now(),
              height: media.height * 0.17,
              width: media.width * 0.2,
              // height: 180,
              // width: 30,
              initialSelectedDate: DateTime.now(),
              daysCount: 8,
              selectionColor: mySecondColor,
              selectedTextColor: Colors.white,
              dateTextStyle: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w600,
                color: Colors.grey,
              ),
              onDateChange: (value) {
                setState(() {
                  _selectedDate = value;
                  // String date = DateFormat('yyyy-MM-dd').format(value);
                });
              },
            ),
          ),
          //Divider
          Container(
            margin: const EdgeInsets.all(10),
            child: Divider(
              color: Colors.grey,
              thickness: 2,
              indent: media.width * 0.1,
              endIndent: media.width * 0.1,
            ),
          ),
          SizedBox(
            height: media.height * 0.67,
            child: BlocBuilder<SessionsBloc, SessionsState>(
              builder: (context, state) {
                if (state is LoadingSessionsState) {
                  return ListView.builder(
                    itemBuilder: (context, int index) {
                      return ShimmerWidgets.sessionCardShimmer();
                    },
                    itemCount: 20,
                  );
                } else if (state is LoadedSessionsState) {
                  return RefreshIndicator(
                      onRefresh: () => _onRefresh(context, date),
                      child: state.sessions.isEmpty
                          ? Center(
                              child: Text(
                                'There is no Sessions yet... ',
                                style: TextStyle(
                                    color: myMainColor,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 19),
                              ),
                            )
                          : Expanded(
                              child: SizedBox(
                                height: media.height * 0.67,
                                child: SessionListForDateWidget(
                                  sessions: state.sessions,
                                ),
                              ),
                            ));
                } else if (state is ErrorSessionsState) {
                  return MessageDisplayWidget(message: state.message);
                }
                return const LoadingWidget();
              },
            ),
          )
          // Number of patients and Picked day
        ],
      ),
    );
  }
}

Future<void> _onRefresh(BuildContext context, String date) async {
  BlocProvider.of<SessionsBloc>(context)
      .add(RefreshAllSessionsByDateEvent(date: date));
}
