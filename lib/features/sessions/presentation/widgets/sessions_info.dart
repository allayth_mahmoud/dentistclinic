// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dentist_clinic/features/sessions/presentation/bloc/add_delete_update_session/add_delete_udate_session_bloc.dart';
import 'package:dentist_clinic/features/sessions/presentation/bloc/sessions/sessions_bloc.dart';
import 'package:dentist_clinic/features/sessions/presentation/widgets/session_list_for_patient_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/utils/constatnts.dart';
import '../../../../core/widgets/loading_widget copy.dart';
import '../../../../core/widgets/message_display_widget.dart';
import '../../../../core/widgets/shimmer_widgets.dart';
import '../../../../core/widgets/snackbar_message.dart';
import 'add_session_bottom_sheet.dart';

class SessionsInfo extends StatefulWidget {
  final String patient_id;

  const SessionsInfo({super.key, required this.patient_id});

  @override
  State<SessionsInfo> createState() => _SessionsInfoState();
}

class _SessionsInfoState extends State<SessionsInfo> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,

      // Sessions ListView
      body: BlocBuilder<SessionsBloc, SessionsState>(
        builder: (context, state) {
          if (state is LoadingSessionsState) {
            return ListView.builder(
              itemBuilder: (context, int index) {
                return ShimmerWidgets.sessionCardShimmer();
              },
              itemCount: 20,
            );
          } else if (state is LoadedSessionsState) {
            return RefreshIndicator(
                onRefresh: () => _onRefresh(context, widget.patient_id),
                child: state.sessions.isEmpty
                    ? Center(
                        child: Text(
                          'There is no Sessions yet... ',
                          style: TextStyle(
                              color: myMainColor,
                              fontWeight: FontWeight.bold,
                              fontSize: 19),
                        ),
                      )
                    : SessionListForPatientWidget(
                        sessions: state.sessions,
                      ));
          } else if (state is ErrorSessionsState) {
            return MessageDisplayWidget(message: state.message);
          }
          return LoadingWidget();
        },
      ),

   

      //Add Button
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showModalBottomSheet(
            context: context,
            builder: (BuildContext context) {
              return BlocConsumer<AddDeleteUdateSessionBloc,
                  AddDeleteUdateSessionState>(listener: (context, state) {
                if (state is MessageAddDeleteUdateSessionState) {
                  SnackBarMessage().showSuccessSnackBar(
                      message: state.message, context: context);

                  BlocProvider.of<SessionsBloc>(context).add(
                      RefreshAllSessionsByPatientEvent(
                          patientId: widget.patient_id));

                  
                } else if (state is ErrorAddDeleteUdateSessionState) {
                  SnackBarMessage().showErrorSnackBar(
                      message: state.message, context: context);
                }
              }, builder: (context, state) {
                if (state is LoadingAddDeleteUdateSessionState) {
                  return LoadingWidget();
                }

                return DateTimePickerBottomSheet(
                  patient_id: widget.patient_id,
                );
              });
            },
          );
        },
        backgroundColor: myMainColor,
        isExtended: true,
        child: const Icon(Icons.add),
      ),
      // drawer: AppDrawer(
      //   id: id,
      // ),
    );
  }
}

Future<void> _onRefresh(BuildContext context, String patientId) async {
  BlocProvider.of<SessionsBloc>(context)
      .add(RefreshAllSessionsByPatientEvent(patientId: patientId));
}
