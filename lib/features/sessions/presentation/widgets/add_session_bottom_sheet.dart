import '../../domain/entities/session.dart';
import '../bloc/add_delete_update_session/add_delete_udate_session_bloc.dart';
import '../bloc/sessions/sessions_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:awesome_dialog/awesome_dialog.dart';

import '../../../../core/utils/constatnts.dart';

class DateTimePickerBottomSheet extends StatefulWidget {
  final String? patient_id;
  DateTimePickerBottomSheet({required this.patient_id});
  @override
  _DateTimePickerBottomSheetState createState() =>
      _DateTimePickerBottomSheetState();
}

class _DateTimePickerBottomSheetState extends State<DateTimePickerBottomSheet> {
  DateTime? _selectedDate;
  TimeOfDay? _selectedTime;
  Future<void> _presentDatePicker() async {
    final pickedDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime.now(),
        lastDate: DateTime.now().add(const Duration(days: 7)));

    if (pickedDate != null) {
      setState(() {
        _selectedDate = pickedDate;
      });
    }
  }

  Future<void> _presentTimePicker() async {
    final pickedTime = await showTimePicker(
      context: context,
      initialEntryMode: TimePickerEntryMode.input,
      initialTime: (_selectedTime != null) ? _selectedTime! : TimeOfDay.now(),
    );

    if (pickedTime != null) {
      setState(() {
        _selectedTime = pickedTime;
      });
    }
  }

  Future<void> _showAlertDialogOnCallBack(
      String title,
      String message,
      DialogType dialogType,
      BuildContext context,
      Function onOkPressed,
      AnimType animType,
      Color? btnOkColor,
      IconData? btnOkIcon) async {
    await AwesomeDialog(
      context: context,
      animType: AnimType.topSlide,
      dialogType: dialogType,
      title: title,
      desc: message,
      btnOkIcon: btnOkIcon,
      btnOkColor: btnOkColor,
      btnOkOnPress: () {
        onOkPressed;
      },
    ).show();
  }

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: const BorderRadius.vertical(top: Radius.circular(20)),
      child: Container(
        padding: const EdgeInsets.all(20),
        height: 325.0,
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 10.0),
            const Text(
              'Date',
              style: TextStyle(fontSize: 16.0, color: myMainColor),
            ),
            const SizedBox(height: 10.0),
            Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              child: ListTile(
                leading: Text(
                  _selectedDate == null
                      ? 'No Chosen Date'
                      : DateFormat('EEE, MMM d').format(_selectedDate!),
                  style: const TextStyle(fontSize: 16.0),
                ),
                trailing: IconButton(
                  icon: const Icon(
                    Icons.date_range,
                    color: mySecondColor,
                  ),
                  onPressed: () {
                    _presentDatePicker();
                  },
                ),
              ),
            ),
            const SizedBox(height: 10.0),
            const Text('Time',
                style: TextStyle(fontSize: 16.0, color: myMainColor)),
            const SizedBox(height: 10.0),
            Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              child: ListTile(
                leading: Text(
                  _selectedTime == null
                      ? 'No Chosen Time'
                      : _selectedTime!.format(context),
                  style: const TextStyle(fontSize: 16.0),
                ),
                trailing: IconButton(
                  icon: const Icon(
                    Icons.access_time,
                    color: mySecondColor,
                  ),
                  onPressed: () {
                    _presentTimePicker();
                  },
                ),
              ),
            ),
            const SizedBox(height: 10.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      backgroundColor: mySecondColor,
                      shape: const StadiumBorder()),
                  onPressed: () async {
                    Session session = Session(
                        sessionId: '',
                        patientId: widget.patient_id as String,
                        sessionDate: '${_selectedDate}${_selectedTime}',
                        status: 1,
                        notices: '',
                        description: '');
                    if ((_selectedDate == null || _selectedTime == null)) {
                      noDateDialoge(context);
                    } else {
                      addSession(session, context);
                      Navigator.of(context).pop();
                      setState(() {});
                    }

                  },
                  child: const Text('Add'),
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: myErrorColor, shape: StadiumBorder()),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('Cancel'),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

void addSession(Session session, context) {
  print('111uuuuuuuuuuuuuuuuuuuuuuuu');
  print(session);

  BlocProvider.of<AddDeleteUdateSessionBloc>(context)
      .add(AddSessionEvent(session: session));
  BlocProvider.of<SessionsBloc>(context)
      .add(GetAllSessionsByPatientEvent(patientId: session.patientId));
}

noDateDialoge(context) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(' '),
        content: Text('Please add a date for this session'),
        actions: <Widget>[
          TextButton(
            child: Text('OK'),
            onPressed: () {
              // do something
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
