import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../../core/utils/constatnts.dart';
import '../../domain/entities/session.dart';

class SessionListWidget extends StatelessWidget {
  final List<Session> scheduleList;
  final selectedDate;
  const SessionListWidget(
      {super.key, required this.scheduleList, this.selectedDate});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              margin: const EdgeInsets.only(left: 20, top: 10),
              child: Text(
                DateFormat("EEEE d").format(
                  selectedDate,
                  /*  (_selectedDate != null) ? _selectedDate : DateTime.now(), */
                ),
                style: const TextStyle(
                  color: mySecondColor,
                  fontFamily: 'lato',
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(left: 20, top: 10, right: 20),
              child: const Text(
                '12 Patients',
                style: TextStyle(color: mySecondColor),
              ),
            
            ),
          ],
        ),
        Expanded(
          child: Container(
            child:
           
                // Sessions List for the day

                ListView.builder(
                    padding: const EdgeInsets.all(10),
                    itemCount: scheduleList.length,
                    itemBuilder: (context, index) {
                      return Container();
                     
                    }),
          ),
        ),
      ],
    );
  }
}
