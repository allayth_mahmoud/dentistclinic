// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dentist_clinic/features/sessions/presentation/bloc/add_delete_update_session/add_delete_udate_session_bloc.dart';
import 'package:dentist_clinic/features/sessions/presentation/widgets/session_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/widgets/loading_widget.dart';
import '../../../../core/widgets/snackbar_message.dart';
import '../../domain/entities/session.dart';
import '../bloc/sessions/sessions_bloc.dart';

class SessionListForPatientWidget extends StatelessWidget {
  final List<Session> sessions;

  const SessionListForPatientWidget({
    Key? key,
    required this.sessions,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AddDeleteUdateSessionBloc, AddDeleteUdateSessionState>(
      listener: (context, state) {
        if (state is MessageAddDeleteUdateSessionState) {
          SnackBarMessage()
              .showSuccessSnackBar(message: state.message, context: context);
          BlocProvider.of<SessionsBloc>(context).add(
              RefreshAllSessionsByPatientEvent(
                  patientId: sessions[0].patientId));

        
        } else if (state is ErrorAddDeleteUdateSessionState) {
          SnackBarMessage()
              .showErrorSnackBar(message: state.message, context: context);
        }
      },
      builder: (context, state) {
        if (state is LoadingAddDeleteUdateSessionState) {
          return LoadingWidget();
        }

        return ListView.builder(
          padding: const EdgeInsets.all(10),
          itemCount: sessions.length,
          itemBuilder: (context, index) {
            return SessionCard(
              patientId: sessions[index].patientId,
              sessionId: sessions[index].sessionId as String,
              notes: sessions[index].notices,
              description: sessions[index].description,
              sessionDate: sessions[index].sessionDate as String,
              status: sessions[index].status,
            );
          },
        );
      },
    );
  }
}
