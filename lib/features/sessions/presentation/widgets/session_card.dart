import '../../domain/entities/session.dart';
import '../bloc/add_delete_update_session/add_delete_udate_session_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
// ignore: depend_on_referenced_packages
// ignore: depend_on_referenced_packages
import '../../../../core/utils/constatnts.dart';

class SessionCard extends StatefulWidget {
  final String patientId;
  final String sessionId;
  final String sessionDate;
  final int status;
  final String? notes;
  final String? description;

  const SessionCard(
      {super.key,
      this.notes,
      this.description,
      required this.patientId,
      required this.sessionId,
      required this.sessionDate,
      required this.status});

  @override
  State<SessionCard> createState() => _SessionCardState();
}

class _SessionCardState extends State<SessionCard> {
//   String dateString = widget.sessionDate.substring(0, 23);
// String timeOfDayString = sessionDate.substring(23);
  bool _isExpaned = false;
  TextEditingController noteController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  @override
  void initState() {
    noteController.text = (widget.notes != null) ? widget.notes! : '';
    descriptionController.text =
        (widget.description != null) ? widget.description! : '';
    super.initState();
  }

  @override
  void dispose() {
    noteController.dispose();
    descriptionController.dispose();
    super.dispose();
  }


  void _saveChanges(
    String notes,
    String description,
    String patientId,
    String sessionId,
  ) {
    Session session = Session(
        sessionId: sessionId,
        patientId: patientId,
        sessionDate: widget.sessionDate,
        status: widget.status,
        description: description,
        notices: notes);

    BlocProvider.of<AddDeleteUdateSessionBloc>(context)
        .add(UpdateSessionEvent(session: session));
  }

  @override
  Widget build(BuildContext context) {
    //var date = DateTime.parse(widget.sessionDate);
    return Column(
      children: [
        Card(
          elevation: _isExpaned ? 0 : 3,
          shape: RoundedRectangleBorder(
            borderRadius: _isExpaned
                ? const BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                  )
                : BorderRadius.circular(20.0),
          ),
          borderOnForeground: true,
          child: ListTile(
            contentPadding: const EdgeInsets.all(10),
            leading: const CircleAvatar(
              backgroundColor: Colors.white,
              child: Icon(
                Icons.medical_services_outlined,
                color: mySecondColor,
              ),
            ),
            // title: Text('Date :${DateFormat('EEE, MMM d').format(date)}'),
            title:
                Text('Date :${widget.sessionDate.substring(0, 10).toString()}'),

            subtitle: Container(
              margin: const EdgeInsets.fromLTRB(0, 5, 5, 5),
              child: Text(
                // 'Time: ${DateFormat.jm().format(date)}',
                'Time: ${widget.sessionDate.substring(23).toString()}',

                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                IconButton(
                  onPressed: () {
                    setState(() {
                      _isExpaned = !_isExpaned;
                    });
                  },
                  icon: _isExpaned
                      ? const Icon(Icons.expand_less)
                      : const Icon(Icons.expand_more),
                ),
                (widget.status == 1)
                    ? const Icon(
                        Icons.circle,
                        color: Colors.green,
                      )
                    : const Icon(Icons.check_circle_outlined),
              ],
            ),
          ),
        ),
        _isExpaned
            ? Container(
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20),
                  ),
                ),
                margin: const EdgeInsets.fromLTRB(5, 0, 5, 15),
                padding: const EdgeInsets.fromLTRB(15, 0, 15, 25),
                child: Column(children: [
                  TextField(
                    decoration: const InputDecoration(
                      label: Text('Description'),
                    ),
                    controller: descriptionController,
                    enabled: (widget.status == 1),
                    maxLines: 2,
                  ),
                  TextField(
                    decoration: const InputDecoration(
                      label: Text('Notices'),
                    ),
                    controller: noteController,
                    enabled: (widget.status == 1),
                    keyboardType: TextInputType.text,
                    maxLines: 2,
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  (widget.status == 1)
                      ? ElevatedButton(
                          onPressed: () {
                            _saveChanges(
                                noteController.text,
                                descriptionController.text,
                                widget.patientId,
                                widget.sessionId);
                          },
                          child: const Text('Save Changes'),
                        )
                      : Container()
                ]),
              )
            : Container(),
        if (widget.status == 1)
          const Divider(
            thickness: 2,
            indent: 30,
            endIndent: 30,
            color: Colors.black,
          )
      ],
    );
  }
}
