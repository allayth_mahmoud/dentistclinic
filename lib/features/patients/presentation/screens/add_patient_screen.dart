
import 'package:awesome_dialog/awesome_dialog.dart';
import '../bloc/add_delete_update_patient/add_delete_update_patient_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/utils/constatnts.dart';
import '../../../../core/widgets/loading_widget.dart';
import '../../../../core/widgets/snackbar_message.dart';
import '../../../medical_report/domain/entities/medical_report.dart';
import '../../domain/entities/patient.dart';
import '../bloc/patients/patient_bloc.dart';
import '../../../sessions/domain/entities/session.dart';
import '../widgets/add_personal_info.dart';

class AddPatientScreen extends StatefulWidget {
  const AddPatientScreen({Key? key, required this.edit, this.patient_id})
      : super(key: key);
  static const routeName = '/stepper-screen';
  final edit;
  final patient_id;

  @override
  State<AddPatientScreen> createState() => _AddPatientScreenState();
}

class _AddPatientScreenState extends State<AddPatientScreen> {
  final personalInfoKey = GlobalKey<FormState>();
  final medicalReportKey = GlobalKey<FormState>();
  // GlobalKey<AddMedicalInfoState> medicalStepKey =
  //     GlobalKey<AddMedicalInfoState>();
  GlobalKey<AddPersonalInfoState> personalStepKey =
      GlobalKey<AddPersonalInfoState>();

  var editedPatient = Patient(
      id: null,
      baseImage: '',
      firstName: '',
      lastName: '',
      cardId: 0,
      phone: 0,
      email: '',
      address: '');
  var _editedSession = Session(
    sessionId: '',
    patientId: '',
    sessionDate: '',
    status: 0,
  );

  MedicalReport editedMedicalReport = MedicalReport(
      id: null,
      patientId: null,
      bloodGroup: '',
      age: 0,
      gender: '',
      allergies: '',
      bloodDisorder: '',
      diabetes: '',
      medications: '');

  DateTime? _selectedDate;
  TimeOfDay? _selectedTime;

  void presentDatePicker() {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: DateTime.now().add(
        const Duration(days: 7),
      ),
    ).then((pickedDate) {
      if (pickedDate == null) {
        return;
      }
      setState(() {
        _selectedDate = pickedDate;
      });
    });
  }

  void presentTimePicker() async {
    await showTimePicker(
      context: context,
      initialTime: (_selectedTime != null) ? _selectedTime! : TimeOfDay.now(),
      initialEntryMode: TimePickerEntryMode.input,
    ).then((pickedTime) {
      if (pickedTime == null) {
        return;
      }
      setState(() {
        _selectedTime = pickedTime;
      });
    });
  }

  int index = 0;
  bool lastStep = false;

  Future<void> _showAlertDialogOnCallBack(
      String title,
      String message,
      DialogType dialogType,
      BuildContext context,
      Function onOkPressed,
      AnimType animType,
      Color? btnOkColor,
      IconData? btnOkIcon) async {
    await AwesomeDialog(
      context: context,
      animType: AnimType.topSlide,
      dialogType: dialogType,
      title: title,
      desc: message,
      btnOkIcon: btnOkIcon,
      btnOkColor: btnOkColor,
      btnOkOnPress: () {
        onOkPressed;
      },
    ).show();
  }

  String image = '';

  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.sizeOf(context);
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        centerTitle: true,
        title: Text(widget.edit ? 'Edit Patient Info' : 'Register Patient'),
        backgroundColor: myMainColor,
        toolbarHeight: 70,
      ),
      body:
          BlocConsumer<AddDeleteUpdatePatientBloc, AddDeleteUpdatePatientState>(
        listener: (context, state) {
          if (state is MessageAddDeleteUpdatePateintState) {
            SnackBarMessage()
                .showSuccessSnackBar(message: state.message, context: context);
            widget.edit ? Navigator.of(context).pop() : null;
            widget.edit ? Navigator.of(context).pop() : null;
            widget.edit ? Navigator.of(context).pop() : null;
            BlocProvider.of<PatientBloc>(context).add(GetAllPatientsEvent());

            
          } else if (state is ErrorAddDeleteUpdatePateintState) {
            SnackBarMessage()
                .showErrorSnackBar(message: state.message, context: context);
          }
        },
        builder: (context, state) {
          if (state is LoadingAddDeleteUpdatePaientState) {
            return LoadingWidget();
          }

          return Column(
            children: [
              SizedBox(
                height: widget.edit ? media.height * 0.68 : media.height * 0.8,
                child: AddPersonalInfo(
                  key: personalStepKey,
                  editedPatient: editedPatient,
                  personalInfoKey: personalInfoKey,
                ),
              ),
              SizedBox(
                height: media.height * 0.08,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    TextButton(
                      onPressed: () {
                        widget.edit ? Navigator.of(context).pop() : null;
                      },
                      child: Text(
                        "Cancel",
                        style: TextStyle(
                            color: widget.edit ? mySecondColor : myMainColor),
                      ),
                    ),
                    ElevatedButton(
                      onPressed: () {
                        addOrUpdatePatient(personalStepKey, editedPatient,
                            context, widget.edit, widget.patient_id);
                      },
                      style: ElevatedButton.styleFrom(
                          backgroundColor: mySecondColor,
                          shape: const StadiumBorder()),
                      child: Text(widget.edit ? "Update" : "Add"),
                    ),
                  ],
                ),
              )
            ],
          );
        },
      ),
    );
  }
}

void addOrUpdatePatient(
    personalStepKey, editedPatient, context, edit, patient_id) {
  //saving the personal information
  personalStepKey.currentState?.widget.personalInfoKey.currentState?.save();

  editedPatient = personalStepKey.currentState!.widget.editedPatient;


  Patient editedPatientFinal = Patient(
      id: edit ? patient_id : '',
      baseImage: editedPatient.baseImage,
      firstName: editedPatient.firstName,
      lastName: editedPatient.lastName,
      cardId: editedPatient.cardId,
      phone: editedPatient.phone,
      email: editedPatient.email,
      address: editedPatient.address);

  edit
      ? BlocProvider.of<AddDeleteUpdatePatientBloc>(context)
          .add(UpdatePatientEvent(patient: editedPatientFinal))
      : BlocProvider.of<AddDeleteUpdatePatientBloc>(context)
          .add(AddPatientEvent(patient: editedPatientFinal));
}
