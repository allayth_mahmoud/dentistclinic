import '../widgets/patient_list_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/widgets/shimmer_widgets.dart';
import '../../../../core/utils/constatnts.dart';
import '../../../../core/widgets/loading_widget copy.dart';
import '../../domain/entities/patient.dart';
import '../bloc/patients/patient_bloc.dart';
import '../../../../core/widgets/message_display_widget.dart';

class PationtsListScreen extends StatefulWidget {
  static const routeName = '/patients_list_screen';

  @override
  State<PationtsListScreen> createState() => _PationtsListScreenState();
}

class _PationtsListScreenState extends State<PationtsListScreen> {
  @override
  void initState() {
    BlocProvider.of<PatientBloc>(context).add(GetAllPatientsEvent());
    super.initState();
  }

  bool isSearched = false;
  String search = '';

  void searchForPatient(value) {
    if (value.isEmpty) {
      isSearched = false;
      search = '';
      setState(() {});
    } else {
      isSearched = true;
      search = value;
      setState(() {});
    }
  }

  List patientList = [
    {
      "firstName": 'jack',
      "lastName": 'stinson',
      "baseImage": 'a;lsdkfjlksjdf',
      "email": "jackaksdfjk@f;lj.com",
    },
    {
      "firstName": 'jack',
      "lastName": 'stinson',
      "baseImage": 'a;lsdkfjlksjdf',
      "email": "jackaksdfjk@f;lj.com",
    },
    {
      "firstName": 'jack',
      "lastName": 'stinson',
      "baseImage": 'a;lsdkfjlksjdf',
      "email": "jackaksdfjk@f;lj.com",
    },
    {
      "firstName": 'jack',
      "lastName": 'stinson',
      "baseImage": 'a;lsdkfjlksjdf',
      "email": "jackaksdfjk@f;lj.com",
    },
    {
      "firstName": 'jack',
      "lastName": 'stinson',
      "baseImage": 'a;lsdkfjlksjdf',
      "email": "jackaksdfjk@f;lj.com",
    },
    {
      "firstName": 'jack',
      "lastName": 'stinson',
      "baseImage": 'a;lsdkfjlksjdf',
      "email": "jackaksdfjk@f;lj.com",
    },
    {
      "firstName": 'jack',
      "lastName": 'stinson',
      "baseImage": 'a;lsdkfjlksjdf',
      "email": "jackaksdfjk@f;lj.com",
    },
    {
      "firstName": 'jack',
      "lastName": 'stinson',
      "baseImage": 'a;lsdkfjlksjdf',
      "email": "jackaksdfjk@f;lj.com",
    },
  ];
  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.sizeOf(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        backgroundColor: myMainColor,
        elevation: 0,
        centerTitle: true,
        title: const Text(
          'All Patients',
          style: TextStyle(
            fontSize: 23,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
      ),
      body: Column(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.vertical(bottom: Radius.circular(31)),
            child: Container(
              height: media.height * 0.15,
              color: myMainColor,
              width: double.infinity,
              alignment: Alignment.topCenter,
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: TextField(
                  onChanged: (value) => {searchForPatient(value)},
                  decoration: const InputDecoration(
                      filled: true,
                      fillColor: Colors.white,
                      alignLabelWithHint: true,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(30)),
                          borderSide: BorderSide.none),
                      hintText: '  Patient name',
                      suffixIcon: Icon(Icons.search),
                      suffixIconColor: mySecondColor),
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
                padding: const EdgeInsets.only(top: 20),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(30),
                      topLeft: Radius.circular(30)),
                  color: Colors.white,
                ),
                child: BlocBuilder<PatientBloc, PatientState>(
                  builder: (context, state) {
                    if (state is LoadingPatientsState) {
                      return ListView.builder(
                        itemBuilder: (context, int index) {
                          return ShimmerWidgets.patientCardShimmer();
                        },
                        itemCount: 20,
                      );
                    } else if (state is LoadedPatientState) {
                      List<Patient> searchedList = state.patients
                          .where((element) => element.firstName
                              .toLowerCase()
                              .startsWith(search))
                          .toList();

                      return RefreshIndicator(
                          onRefresh: () => _onRefresh(context),
                          child: state.patients.isEmpty
                              ? Center(
                                  child: Text(
                                    'There is no medical report... please add one',
                                    style: TextStyle(
                                        color: myMainColor,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 19),
                                  ),
                                )
                              : PatientListWidget(
                                  patient: isSearched
                                      ? searchedList
                                      : state.patients,
                                ));
                    } else if (state is ErrorPatientsState) {
                      return MessageDisplayWidget(message: state.message);
                    }
                    return LoadingWidget();
                  },
                )),
          )
        ],
      ),
    );
  }
}

Future<void> _onRefresh(BuildContext context) async {
  BlocProvider.of<PatientBloc>(context).add(RefreshPatientsEvent());
}
