import '../../../medical_report/presentation/bloc/medical_reports/medical_reports_bloc.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/utils/constatnts.dart';
import '../../../sessions/presentation/bloc/sessions/sessions_bloc.dart';
import '../widgets/personal_info.dart';
import 'package:flutter/material.dart';

import '../../../../core/widgets/custom_shape.dart';
import '../../../medical_report/presentation/widgets/medical_info.dart';
import '../../../sessions/presentation/widgets/sessions_info.dart';

class PatientInfoScreen extends StatefulWidget {
  static const routeName = 'patient_info_screen';

  @override
  State<PatientInfoScreen> createState() => _PatientInfoScreenState();
}

class _PatientInfoScreenState extends State<PatientInfoScreen> {
  @override
  void initState() {
    super.initState();
  }

  int _selectedIndex = 0;

  void _selectPage(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  bool isStatementExecuted = false;

  @override
  Widget build(BuildContext context) {
    final args =
        ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;
    if (!isStatementExecuted) {
      BlocProvider.of<SessionsBloc>(context).add(GetAllSessionsByPatientEvent(
          patientId: args['personal_info'].id as String));
      BlocProvider.of<MedicalReportsBloc>(context).add(
          GetMedicalReportByPatientEvent(
              patientId: args['personal_info'].id as String));
      isStatementExecuted = true;
    }

    List<Map<String, Object>> _pages = [
      {
        'page': PersonalInfo(personalInfo: args['personal_info']),
        'title': 'Personal',
      },
      {
        'page': MedicalInfo(patient_id: args['personal_info'].id),
        'title': 'Medical',
      },
      {
        'page': SessionsInfo(patient_id: args['personal_info'].id),
        'title': 'Sessions',
      },
    ];
    print('ddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd');
    print(args['personal_info']);

    var media = MediaQuery.sizeOf(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(

        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.of(context).pop();
            Navigator.of(context).pop();
          },
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
        toolbarHeight: 200,
        flexibleSpace: ClipPath(
          clipper: CustomShape(),
          child: Container(
            height: 250,
            width: MediaQuery.of(context).size.width,
            color: myMainColor,
            // Patient Photo and Name
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircleAvatar(
                  radius: 20,
                  child: ClipRRect(
                    borderRadius: const BorderRadius.all(Radius.circular(100)),
                    child: Image.network(
                      'snapshot.data!.baseImage!',
                      fit: BoxFit.cover,
                      width: 100,
                      height: 100,
                      errorBuilder: (context, error, stackTrace) => const Icon(
                        Icons.account_circle,
                        size: 100,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
               
                Container(
                  padding: const EdgeInsets.all(10),
                  child: Text(
                    '${args['personal_info'].firstName} ${args['personal_info'].lastName  }',
                    style: const TextStyle(
                      fontSize: 20,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      //drawer: MainDrawer(),
      body: SizedBox(
          height: media.height * 0.77,
          child: _pages[_selectedIndex]['page'] as Widget),
      bottomNavigationBar: BottomNavigationBar(
        onTap: _selectPage,
        backgroundColor: myMainColor,
        unselectedItemColor: Colors.white,
        selectedItemColor: mySecondColor,
        currentIndex: _selectedIndex,
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Personal',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.medical_information),
            label: 'Medical',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.timelapse),
            label: 'Sessions',
          ),
        ],
      ),
    );
  }
}
