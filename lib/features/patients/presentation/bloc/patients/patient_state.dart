part of 'patient_bloc.dart';

abstract class PatientState extends Equatable {
  const PatientState();

  @override
  List<Object> get props => [];
}

class PatientInitial extends PatientState {}

class LoadingPatientsState extends PatientState {}

class LoadedPatientState extends PatientState {
  final List<Patient> patients;

   LoadedPatientState({required this.patients});
   
   @override
  List<Object> get props => [patients];
  
}

class ErrorPatientsState extends PatientState {
  final String message;

   ErrorPatientsState({required this.message});

   @override
  List<Object> get props => [message];
}
