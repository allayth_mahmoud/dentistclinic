// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../../../../core/error/failures.dart';
import '../../../../../core/strings/failures.dart';
import '../../../domain/entities/patient.dart';
import '../../../domain/usecases/get_all_patients.dart';

part 'patient_event.dart';
part 'patient_state.dart';

class PatientBloc extends Bloc<PatientEvent, PatientState> {
  final GetAllPatientUseCase getAllPatientUseCase;

  PatientBloc(
    this.getAllPatientUseCase,
  ) : super(PatientInitial()) {
    on<PatientEvent>((event, emit) async {
      if (event is GetAllPatientsEvent) {
        emit(LoadingPatientsState());

        final failureOrPatients = await getAllPatientUseCase();
        emit(_mapFailureOrPatientsToState(failureOrPatients));
      } else if (event is RefreshPatientsEvent) {
        emit(LoadingPatientsState());

        final failureOrPatients = await getAllPatientUseCase();
        emit(_mapFailureOrPatientsToState(failureOrPatients));
      }
    });
  }

  PatientState _mapFailureOrPatientsToState(
      Either<Failure, List<Patient>> either) {
    return either.fold(
      (failure) => ErrorPatientsState(message: _mapFailureToMessage(failure)),
      (patients) => LoadedPatientState(
        patients: patients,
      ),
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case EmptyCacheFailure:
        return EMPTY_CACHE_FAILURE_MESSAGE;
      case OfflineFailure:
        return OFFLINE_FAILURE_MESSAGE;
      default:
        return "Unexpected Error , Please try again later .";
    }
  }
}
