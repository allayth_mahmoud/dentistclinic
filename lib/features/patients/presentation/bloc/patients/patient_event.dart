part of 'patient_bloc.dart';

abstract class PatientEvent extends Equatable {
  const PatientEvent();

  @override
  List<Object> get props => [];
}

class GetAllPatientsEvent extends PatientEvent {}

class RefreshPatientsEvent extends PatientEvent {}
