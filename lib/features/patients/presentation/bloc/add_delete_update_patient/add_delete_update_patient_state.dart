part of 'add_delete_update_patient_bloc.dart';

abstract class AddDeleteUpdatePatientState extends Equatable {
  const AddDeleteUpdatePatientState();

  @override
  List<Object> get props => [];
}

class AddDeleteUpdatePatientInitial extends AddDeleteUpdatePatientState {}

class LoadingAddDeleteUpdatePaientState extends AddDeleteUpdatePatientState {}

class ErrorAddDeleteUpdatePateintState extends AddDeleteUpdatePatientState {
  final String message;
  ErrorAddDeleteUpdatePateintState({required this.message});

  @override
  List<Object> get props => [message];
}

class MessageAddDeleteUpdatePateintState extends AddDeleteUpdatePatientState {
  final String message;
  MessageAddDeleteUpdatePateintState({required this.message});

  @override
  List<Object> get props => [message];
}
