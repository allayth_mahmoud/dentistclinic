import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../../../../core/error/failures.dart';
import '../../../../../core/strings/failures.dart';
import '../../../../../core/strings/messages.dart';
import '../../../domain/entities/patient.dart';
import '../../../domain/usecases/add_patient.dart';
import '../../../domain/usecases/delete_pateint.dart';
import '../../../domain/usecases/update_patient.dart';

part 'add_delete_update_patient_event.dart';
part 'add_delete_update_patient_state.dart';

class AddDeleteUpdatePatientBloc
    extends Bloc<AddDeleteUpdatePatientEvent, AddDeleteUpdatePatientState> {
  final AddPatientUseCase addPatientUseCase;
  final UpdatePatientUseCase updatePatientUseCase;
  final DeletePatientUseCase deletePatientUseCase;

  AddDeleteUpdatePatientBloc(
      {required this.addPatientUseCase,
      required this.updatePatientUseCase,
      required this.deletePatientUseCase})
      : super(AddDeleteUpdatePatientInitial()) {
    on<AddDeleteUpdatePatientEvent>((event, emit) async {
      if (event is AddPatientEvent) {
        emit(LoadingAddDeleteUpdatePaientState());
        print('1gggggggggggggggggggggggggggg');

        final failureOrAddPatient = await addPatientUseCase(event.patient);
                print('2gggggggggggggggggggggggggggg');

        emit(_eitherDoneMessageOrErrorState(
            failureOrAddPatient, ADD_SUCCESS_MESSAGE));
      } else if (event is UpdatePatientEvent) {
        emit(LoadingAddDeleteUpdatePaientState());

        final failureOrDoneMessage = await updatePatientUseCase(event.patient);

        emit(
          _eitherDoneMessageOrErrorState(
              failureOrDoneMessage, UPDATE_SUCCESS_MESSAGE),
        );
      } else if (event is DeletePatientEvent) {
        emit(LoadingAddDeleteUpdatePaientState());

        final failureOrDoneMessage =
            await deletePatientUseCase(event.patientId);

        emit(
          _eitherDoneMessageOrErrorState(
              failureOrDoneMessage, DELETE_SUCCESS_MESSAGE),
        );
      }
    });
  }

  AddDeleteUpdatePatientState _eitherDoneMessageOrErrorState(
      Either<Failure, Unit> either, String message) {
    return either.fold(
      (failure) => ErrorAddDeleteUpdatePateintState(
        message: _mapFailureToMessage(failure),
      ),
      (_) => MessageAddDeleteUpdatePateintState(message: message),
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case OfflineFailure:
        return OFFLINE_FAILURE_MESSAGE;
      default:
        return "Unexpected Error , Please try again later .";
    }
  }
}
