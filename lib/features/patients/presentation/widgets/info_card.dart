import 'package:flutter/material.dart';

class InfoCard extends StatelessWidget {
  const InfoCard({
    Key? key,
    required this.icon,
    required this.title,
    required this.value,
  }) : super(key: key);
  final Icon icon;
  final String title;
  final String? value;

  @override
  Widget build(BuildContext context) {
    Size media = MediaQuery.sizeOf(context);

    return Card(
      
      child: Container(
        height: 65,
        padding: const EdgeInsets.all(6),
        child: Row(
          children: [
            Row(
              children: [
                icon,
                SizedBox(
                  width: media.width * 0.02,
                ),
                Text(
                  title,
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            SizedBox(
              width: media.width * 0.02,
            ),
            Text(
              value!,
              style: TextStyle(fontSize: 16),
            ),
          ],
        ),
      ),
    );
  }
}
