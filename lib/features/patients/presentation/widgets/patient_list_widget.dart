import '../bloc/add_delete_update_patient/add_delete_update_patient_bloc.dart';
import '../bloc/patients/patient_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'patient_card.dart';
import '../../domain/entities/patient.dart';
import '../screens/patient_info_screen.dart';

class PatientListWidget extends StatefulWidget {
  final List<Patient> patient;
  const PatientListWidget({
    Key? key,
    required this.patient,
  }) : super(key: key);

  @override
  State<PatientListWidget> createState() => _PatientListWidgetState();
}

class _PatientListWidgetState extends State<PatientListWidget> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: const EdgeInsets.all(10),
      itemBuilder: (context, index) => PatientCard(
        onDelete: () async {
          BlocProvider.of<AddDeleteUpdatePatientBloc>(context).add(
              DeletePatientEvent(
                  patientId: widget.patient[index].id as String));
          BlocProvider.of<PatientBloc>(context).add(RefreshPatientsEvent());
        },
        firstName: widget.patient[index].firstName,
        lastName: widget.patient[index].lastName,
        imagePath: widget.patient[index].baseImage,
        email: widget.patient[index].email,
        onTap: () {
          print('sssssssssssssssssssssssssssssssssssssss');
          print(widget.patient[index].id);
          Map<String, dynamic> arguments = {
            'personal_info': widget.patient[index],
          };
          // BlocProvider.of<SessionsBloc>(context).add(
          //     GetAllSessionsByPatientEvent(
          //         patientId: patient[index].id as String));
          Navigator.of(context).pushNamed(
            PatientInfoScreen.routeName,
            arguments: arguments,
          );
          Navigator.of(context)
              .pushNamed(PatientInfoScreen.routeName, arguments: arguments);
        },
      ),
      itemCount: widget.patient.length,
    );
  }
}
