import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:image_picker/image_picker.dart';

import '../../domain/entities/patient.dart';

class AddPersonalInfo extends StatefulWidget {
  Patient editedPatient;
  GlobalKey<FormState> personalInfoKey;
  AddPersonalInfo({
    super.key,
    required this.editedPatient,
    required this.personalInfoKey,
  });

  @override
  State<AddPersonalInfo> createState() => AddPersonalInfoState();
}

class AddPersonalInfoState extends State<AddPersonalInfo> {
  File? imageFile;
  String? baseImage = '';

// compress function is here
  Future<List<int>> compressImage(String imagePath) async {
    final bytes = await FlutterImageCompress.compressWithFile(
      imagePath,
      quality: 20,
    );

    return bytes as List<int>;
  }

  // Here should be a variable that holds the image string
  void _takePhoto(ImageSource img) async {
    var pickedImage =
        await ImagePicker().pickImage(source: img, imageQuality: 30);
    if (pickedImage != null) {
      // String imageName = pickedImage.path.split("/").last;
      List<int> imageBytes = File(pickedImage.path).readAsBytesSync();
      imageFile = File(pickedImage.path);
      setState(() {
        baseImage = pickedImage.path;
        // baseImage = base64Encode(imageBytes);
        print(baseImage);
      });
    } else {}
  }

  //bottom sheet
  Widget bottomSheet() {
    return Container(
      height: 140,
      width: MediaQuery.of(context).size.width,
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: Column(children: [
        const Text(
          'Choose Photo',
          style: TextStyle(fontSize: 20),
        ),
        const SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            TextButton.icon(
              onPressed: () {
                _takePhoto(ImageSource.camera);
                // create _edited patient item and add the photo to it
              },
              icon: const Icon(Icons.camera),
              label: const Text('Camera'),
            ),
            TextButton.icon(
              onPressed: () {
                _takePhoto(ImageSource.gallery);
              },
              icon: const Icon(Icons.image),
              label: const Text('Gallery'),
            )
          ],
        ),
        Center(
          child: ElevatedButton(
            child: const Text('Ok'),
            onPressed: () => Navigator.of(context).pop(),
          ),
        )
      ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Form(
        key: widget.personalInfoKey,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(10, 25, 10, 10),
          child: SingleChildScrollView(
              child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 25),
                child: Card(
                  elevation: 8,
                  child: TextFormField(
                    decoration:
                        const InputDecoration(labelText: '    First Name'),
                    textInputAction: TextInputAction.next,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please enter a value';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      widget.editedPatient = Patient(
                          id: widget.editedPatient.id,
                          baseImage: baseImage,
                          firstName: value.toString(),
                          lastName: widget.editedPatient.lastName,
                          cardId: widget.editedPatient.cardId,
                          phone: widget.editedPatient.phone,
                          email: widget.editedPatient.email,
                          address: widget.editedPatient.address);
                    },
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 25),
                child: Card(
                  elevation: 8,
                  child: TextFormField(
                    decoration:
                        const InputDecoration(labelText: '    Last Name'),
                    textInputAction: TextInputAction.next,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please enter a value';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      widget.editedPatient = Patient(
                          id: widget.editedPatient.id,
                          baseImage: baseImage,
                          firstName: widget.editedPatient.firstName,
                          lastName: value.toString(),
                          cardId: widget.editedPatient.cardId,
                          phone: widget.editedPatient.phone,
                          email: widget.editedPatient.email,
                          address: widget.editedPatient.address);
                    },
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 25),
                child: Card(
                  elevation: 8,
                  child: TextFormField(
                    decoration:
                        const InputDecoration(labelText: '    Email Address'),
                    textInputAction: TextInputAction.next,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please enter a value';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      widget.editedPatient = Patient(
                          id: widget.editedPatient.id,
                          baseImage: baseImage,
                          firstName: widget.editedPatient.firstName,
                          lastName: widget.editedPatient.lastName,
                          cardId: widget.editedPatient.cardId,
                          phone: widget.editedPatient.phone,
                          email: value.toString(),
                          address: widget.editedPatient.address);
                    },
                    keyboardType: TextInputType.emailAddress,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 25),
                child: Card(
                  elevation: 8,
                  child: TextFormField(
                    decoration:
                        const InputDecoration(labelText: '    Phone Number'),
                    textInputAction: TextInputAction.next,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please enter a value';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      widget.editedPatient = Patient(
                          id: widget.editedPatient.id,
                          baseImage: baseImage,
                          firstName: widget.editedPatient.firstName,
                          lastName: widget.editedPatient.lastName,
                          cardId: widget.editedPatient.cardId,
                          phone: /* value!  */ int.parse(value!),
                          email: widget.editedPatient.email,
                          address: widget.editedPatient.address);
                    },
                    keyboardType: TextInputType.number,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 25),
                child: Card(
                  elevation: 8,
                  child: TextFormField(
                    decoration:
                        const InputDecoration(labelText: '    Living Address'),
                    textInputAction: TextInputAction.next,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please enter a value';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      widget.editedPatient = Patient(
                          id: widget.editedPatient.id,
                          baseImage: baseImage,
                          firstName: widget.editedPatient.firstName,
                          lastName: widget.editedPatient.lastName,
                          cardId: widget.editedPatient.cardId,
                          phone: widget.editedPatient.phone,
                          email: widget.editedPatient.email,
                          address: value.toString());
                    },
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 25),
                child: Card(
                  elevation: 8,
                  child: TextFormField(
                    decoration: const InputDecoration(labelText: '    Card Id'),
                    textInputAction: TextInputAction.next,
                    validator: (value) {
                      if (value!.isEmpty) return 'Please enter a value';
                      return null;
                    },
                    onSaved: (value) {
                      widget.editedPatient = Patient(
                          id: widget.editedPatient.id,
                          baseImage: baseImage,
                          firstName: widget.editedPatient.firstName,
                          lastName: widget.editedPatient.lastName,
                          cardId: /* value! */ int.parse(value!),
                          phone: widget.editedPatient.phone,
                          email: widget.editedPatient.email,
                          address: widget.editedPatient.address);
                    },
                    keyboardType: TextInputType.number,
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 25),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Card(
                      elevation: 8,
                      child: Container(
                        height: 100,
                        width: 100,
                        margin: const EdgeInsets.only(top: 8, right: 10),
                        decoration: BoxDecoration(
                            //border: Border.all(width: 1, color: Colors.grey),
                            ),
                        child: imageFile == null
                            ? Center(
                                child: const Text(
                                  'Photo Privew',
                                ),
                              )
                            : FittedBox(
                                //  Image.network(
                                //     _imageUrlController.text),
                                fit: BoxFit.contain,
                                child: Image.file(imageFile!),
                              ),
                      ),
                    ),
                    Expanded(
                      child: Card(
                        elevation: 8,
                        child: TextFormField(
                          decoration:
                              const InputDecoration(labelText: '    Image URL'),
                          textInputAction: TextInputAction.next,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Please enter a Url';
                            }
                            return null;
                          },
                          onSaved: (value) {
                            widget.editedPatient = Patient(
                                id: widget.editedPatient.id,
                                baseImage: value.toString(),
                                firstName: widget.editedPatient.firstName,
                                lastName: widget.editedPatient.lastName,
                                cardId: widget.editedPatient.cardId,
                                phone: widget.editedPatient.phone,
                                email: widget.editedPatient.email,
                                address: widget.editedPatient.address);
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )),
        ),
      ),
    );
  }
}
