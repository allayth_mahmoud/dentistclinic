// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dentist_clinic/features/patients/presentation/screens/add_patient_screen.dart';
import 'package:flutter/material.dart';

import 'package:dentist_clinic/features/patients/domain/entities/patient.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/utils/constatnts.dart';
import '../bloc/add_delete_update_patient/add_delete_update_patient_bloc.dart';
import 'info_card.dart';

class PersonalInfo extends StatelessWidget {
  static const routeName = '/personal-info-screen';

  final Patient personalInfo;
  const PersonalInfo({
    Key? key,
    required this.personalInfo,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.sizeOf(context);
    return Scaffold(
        body: SizedBox(
      height: media.height * 0.83,
      child: ListView(
        children: [
          SizedBox(
            height: media.height * 0.05,
            child: Row(
              // mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Icon(
                  Icons.edit,
                  color: mySecondColor,
                ),
                TextButton(
                  onPressed: () {
                    updatePersonalInfoDialog(
                        context, personalInfo.id as String, true);
                 
                  },
                  child: const Text(
                    "Edit Info",
                    textAlign: TextAlign.start,
                    style: TextStyle(color: mySecondColor),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: media.height * 0.12,
            child: InfoCard(
                icon: const Icon(
                  Icons.person,
                  color: mySecondColor,
                ),
                title: 'First Name: ',
                value: personalInfo.firstName),
          ),
          SizedBox(
            height: media.height * 0.12,
            child: InfoCard(
                icon: const Icon(
                  Icons.person,
                  color: mySecondColor,
                ),
                title: 'Last Name: ',
                value: personalInfo.lastName),
          ),
          SizedBox(
            height: media.height * 0.12,
            child: InfoCard(
                icon: const Icon(
                  Icons.phone,
                  color: mySecondColor,
                ),
                title: 'Phone Number: ',
                value: personalInfo.phone.toString()),
          ),
          SizedBox(
            height: media.height * 0.12,
            child: InfoCard(
                icon: const Icon(
                  Icons.email_outlined,
                  color: mySecondColor,
                ),
                title: 'Email: ',
                value: personalInfo.email),
          ),
          SizedBox(
            height: media.height * 0.12,
            child: InfoCard(
                icon: const Icon(
                  Icons.location_on,
                  color: mySecondColor,
                ),
                title: 'Address: ',
                value: personalInfo.address),
          ),
          SizedBox(
            height: media.height * 0.12,
            child: InfoCard(
                icon: const Icon(
                  Icons.star_border_outlined,
                  color: mySecondColor,
                ),
                title: 'Card Id: ',
                value: personalInfo.cardId.toString()),
          ),
        ],
      ),
    ));
  }
}

updatePersonalInfoDialog(BuildContext context, String patient_id, bool edit) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Container(
          // height: 200.0,
          // width: 200.0,
          padding: EdgeInsets.all(20.0),
          child: AddPatientScreen(
             edit: edit,
            patient_id: patient_id,
          ),
        ),
      );
    },
  );
}

void addPatient(personalStepKey, editedPatient, context, patient_id) {
  //saving the personal information
  personalStepKey.currentState?.widget.personalInfoKey.currentState?.save();

  editedPatient = personalStepKey.currentState!.widget.editedPatient;


  BlocProvider.of<AddDeleteUpdatePatientBloc>(context)
      .add(UpdatePatientEvent(patient: editedPatient));
}
