import 'package:flutter/material.dart';

class PatientCard extends StatelessWidget {
  final String? imagePath;

  final String firstName;
  final String lastName;

  final String email;
  final Function onTap;
  final Function onDelete;

  PatientCard(
      {required this.firstName,
      required this.lastName,
      required this.imagePath,
      required this.email,
      required this.onTap,
      required this.onDelete});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onTap();
      },
      splashColor: Colors.white12,
      child: Card(
        margin: const EdgeInsets.all(10.0),
        borderOnForeground: true,
        shape: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(8.0)),
        child: ListTile(
          contentPadding: const EdgeInsets.all(8.0),
          leading: Container(
           
              margin: const EdgeInsets.only(left: 5),
              height: 130,
              width: 50,
              child: (imagePath == null)
                  ? const Icon(Icons.account_circle, size: 55)
                  : CircleAvatar(
                      backgroundImage: NetworkImage(imagePath!),
                    )),
          title: Text(
            '$firstName $lastName',
            style: const TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: 17,
              fontFamily: 'Lato',
            ),
          ),
          subtitle: Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Text(
              email,
              style: const TextStyle(color: Colors.black),
            ),
          ),
          trailing: IconButton(
            icon: const Icon(
              Icons.delete,
              color: Colors.red,
            ),
            onPressed: () {
              onDelete();
            },
          ),
        ),
      ),
    );
  }
}
