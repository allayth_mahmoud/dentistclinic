import 'package:equatable/equatable.dart';

class Patient extends Equatable {
  final String? id;

  final String firstName;
  final String lastName;
  final int cardId;
  final int phone;
  final String email;
  final String address;
  final String? baseImage;

  Patient({
    required this.id,
    required this.baseImage,
    required this.firstName,
    required this.lastName,
    required this.cardId,
    required this.phone,
    required this.email,
    required this.address,
  });

  @override
  List<Object?> get props =>
      [id, baseImage, firstName, lastName, cardId, phone, email, address];
}
