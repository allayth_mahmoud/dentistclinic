

 
import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../entities/patient.dart';

abstract class PatientRepository{

  Future<Either<Failure, List<Patient>>> getAllPatient();
  Future<Either<Failure, Unit>> deletePatient(String id);
  Future<Either<Failure, Unit>> updatePatient(Patient patient);
  Future<Either<Failure, Unit>> addPatient(Patient patient);

}