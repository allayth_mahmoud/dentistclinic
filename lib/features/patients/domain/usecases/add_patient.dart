import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../entities/patient.dart';
import '../repositories/patient_repositories.dart';

class AddPatientUseCase {
  final PatientRepository repository;
  AddPatientUseCase(this.repository);

  Future<Either<Failure, Unit>> call(Patient patient) async {
    print('add_patient_ usecase');
    return await repository.addPatient(patient);
  }
}
