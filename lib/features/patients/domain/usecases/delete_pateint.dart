import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../repositories/patient_repositories.dart';

class DeletePatientUseCase {
  final PatientRepository repository;
  DeletePatientUseCase(this.repository);

  Future<Either<Failure, Unit>> call(String id) async {
    return await repository.deletePatient(id);
  }
}
