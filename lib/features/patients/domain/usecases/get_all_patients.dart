import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../entities/patient.dart';
import '../repositories/patient_repositories.dart';

class GetAllPatientUseCase {
  final PatientRepository repository;
  GetAllPatientUseCase(this.repository);

  Future<Either<Failure, List<Patient>>> call( ) async {
    return await repository.getAllPatient( );
  }
}
