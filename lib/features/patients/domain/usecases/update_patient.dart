import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../entities/patient.dart';
import '../repositories/patient_repositories.dart';

class UpdatePatientUseCase {
  final PatientRepository repository;
  UpdatePatientUseCase(this.repository);

  Future<Either<Failure, Unit>> call(Patient patient) async {

 
    return await repository.updatePatient(patient);
  }
}
