import '../../domain/entities/patient.dart';

class PatientModel  extends Patient {
  final String? id;

  final String firstName;
  final String lastName;
  final int cardId;
  final int phone;
  final String email;
  final String address;
  final String? baseImage;

  PatientModel({
    required this.id,
    required this.baseImage,
    required this.firstName,
    required this.lastName,
    required this.cardId,
    required this.phone,
    required this.email,
    required this.address,
  }) : super(id: '', baseImage: '', firstName: '', lastName: '', cardId: 0, phone: 0, email: '', address: '');

  factory PatientModel.fromMap(Map<String, dynamic> json) {
    return PatientModel(
      id: json['id'],
      baseImage: json['profile_image'],
      firstName: json['first_name'],
      lastName: json['last_name'],
      cardId: int.parse(json['card_id']) ,
      phone: /* json['phone']  */ int.parse(json['phone']),
      email: json['email'],
      address: json['address'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "profile_image": baseImage,
      "first_name": firstName,
      "last_name": lastName,
      "card_id": cardId,
      "phone": phone,
      "email": email,
      "address": address,
    };
  }
}





// {"1": {"address": "asdfsdf", "card_id": "12", "email": "dfsadf@sdf", "first_name": "jack", "last_name": "stinson", "phone": "12313423", 'profile_image': "adsfsdfsadf"}, 
// "aaaaaa": {"address": "asdfsdf", "card_id": "12", "email": "dfsadf@sdf", "first_name": "jack", "last_name": "stinson", "phone": "12313423", "profile_image": "adsfsdfsadf"}}

// [
//  {"id": 1,"address": "asdfsdf", "card_id": "12", "email": "dfsadf@sdf", "first_name": "jack", "last_name": "stinson", "phone": "12313423", 'profile_image': "adsfsdfsadf"}, {
//  "id":2, "address": "asdfsdf", "card_id": "12", "email": "dfsadf@sdf", "first_name": "jack", "last_name": "stinson", "phone": "12313423", "profile_image": "adsfsdfsadf"}]