import 'package:dartz/dartz.dart';

import '../../../../core/error/exception.dart';
import '../../../../core/error/failures.dart';
import '../../../../core/network/network_info.dart';
import '../../domain/entities/patient.dart';
import '../../domain/repositories/patient_repositories.dart';
import '../datasources/patient_local_data_source.dart';
import '../datasources/patient_remote_data_source.dart';
import '../models/patient_model.dart';

class PatientRepositoryImpl implements PatientRepository {
  final PatientRemoteDataSource remoteDataSource;
  final PatientLocalDataSource localDataSource;
  final NetworkInfo networkInfo;

  PatientRepositoryImpl(
      {required this.remoteDataSource,
      required this.localDataSource,
      required this.networkInfo});

  @override
  Future<Either<Failure, List<Patient>>> getAllPatient() async {
    if (await networkInfo.isConnected) {
      try {
        final remotePatients = await remoteDataSource.getAllPatients();
        localDataSource.cachPateints(remotePatients);
        return Right(remotePatients);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      try {
        final localPatients = await localDataSource.getCachedPatients();
        return Right(localPatients.cast<Patient>());
      } on EmptyCacheException {
        return Left(EmptyCacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, Unit>> addPatient(Patient patient) async {
    final PatientModel patientModel = PatientModel(
        id: null,
        baseImage: patient.baseImage,
        firstName: patient.firstName,
        lastName: patient.lastName,
        cardId: patient.cardId,
        phone: patient.phone,
        email: patient.email,
        address: patient.address);
            print('add_patient_ repo impl');

    return await _getMessage(() {
      return   remoteDataSource.addPatient(patientModel);
    });
  }

  @override
  Future<Either<Failure, Unit>> updatePatient(Patient patient) async {
    final PatientModel patientModel = PatientModel(
        id: patient.id,
        baseImage: patient.baseImage,
        firstName: patient.firstName,
        lastName: patient.lastName,
        cardId: patient.cardId,
        phone: patient.phone,
        email: patient.email,
        address: patient.address);
         return await _getMessage(() {
      return remoteDataSource.updatePatient(patientModel);
    });
   }

  @override
  Future<Either<Failure, Unit>> deletePatient(String id) async{
     return await _getMessage(() {
      return remoteDataSource.deletePatient(id);
    });
  }

  Future<Either<Failure, Unit>> _getMessage(
      Future<Unit> Function() deleteOrUpdateOrAddPatient) async {
    if (await networkInfo.isConnected) {
      try {
        await deleteOrUpdateOrAddPatient();
        return Right(unit);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(OfflineFailure());
    }
  }
}
