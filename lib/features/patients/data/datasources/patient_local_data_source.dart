// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../core/error/exception.dart';
import '../models/patient_model.dart';

abstract class PatientLocalDataSource {
  Future<List<PatientModel>> getCachedPatients();
  Future<Unit> cachPateints(List<PatientModel> patientsModel);
}

const cached_patients = 'CACHED_PATIENTS';

class PatientLocalDataSourceImpl implements PatientLocalDataSource {
    final SharedPreferences sharedPreferences;
  PatientLocalDataSourceImpl({
    required this.sharedPreferences,
  });
  @override
  Future<Unit> cachPateints(List<PatientModel> patientsModel) {
    List patientModelsToJson = patientsModel
        .map<Map>((patientModel) => patientModel.toJson())
        .toList();
    sharedPreferences.setString(
        cached_patients, json.encode(patientModelsToJson));
    return Future.value(unit);
  }

  @override
  Future<List<PatientModel>> getCachedPatients() {
    final jsonString = sharedPreferences.getString(cached_patients);
    if (jsonString != null) {
      List decodedJsonData = json.decode(jsonString);
      List<PatientModel> jsonToPatientModel = decodedJsonData
          .map((jsonPatientModel) => PatientModel.fromMap(jsonPatientModel))
          .toList();
      return Future.value(jsonToPatientModel);
    } else {
      throw EmptyCacheException();
    }
  }
}
