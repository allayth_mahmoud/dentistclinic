// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:dentist_clinic/core/utils/constatnts.dart';
import 'package:http/http.dart' as http;

import '../../../../core/error/exception.dart';
import '../../../../core/utils/my_functions.dart';
import '../models/patient_model.dart';

abstract class PatientRemoteDataSource {
  Future<List<PatientModel>> getAllPatients();
  Future<Unit> deletePatient(String patientId);
  Future<Unit> updatePatient(PatientModel patient);
  Future<Unit> addPatient(PatientModel patient);
}

const base_url = 'http://localhost:3000/';
final headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
};

class PatientRemoteDataSourceImpl implements PatientRemoteDataSource {
  final http.Client client;
  PatientRemoteDataSourceImpl({
    required this.client,
  });

  @override
  Future<List<PatientModel>> getAllPatients() async {
    print('dddddddddddddddddddddd');
    final response = await client.get(Uri.parse(baseUrl + '/patient.json'),
        headers: headers);
    print(json.decode(response.body));
    var responseBody = transform(json.decode(response.body));
    print(responseBody);
    if (response.statusCode == 200) {
      final decodedJson = responseBody;
      final List<PatientModel> patientsModel = decodedJson
          .map<PatientModel>(
              (jsonPatientModel) => PatientModel.fromMap(jsonPatientModel))
          .toList();

      return patientsModel;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<Unit> addPatient(PatientModel patient) async {
    final body = {
      // "id": patient.id,
      "profile_image": patient.baseImage,
      "first_name": patient.firstName,
      "last_name": patient.lastName,
      "card_id": patient.cardId.toString(),
      "phone": patient.phone.toString(),
      "email": patient.email,
      "address": patient.address,
    };
    print('ffffffffffffffffffffffffff');
    final response = await client.post(Uri.parse(baseUrl + '/patient.json'),
        body: json.encode(body), headers: headers);
    print(response.body);
    print('222ffffffffffffffffffffffffff');

    if (response.statusCode == 200) {
      return Future.value(unit);
    } else {
      throw ServerException();
    }
  }

  @override
  Future<Unit> updatePatient(PatientModel patient) async {
    final patientId = patient.id.toString();
    final body = {
      "profile_image": patient.baseImage,
      "first_name": patient.firstName,
      "last_name": patient.lastName,
      "card_id": patient.cardId.toString(),
      "phone": patient.phone.toString(),
      "email": patient.email,
      "address": patient.address,
    };
    print('1ffffffffffffffffffff');
    final response = await client.put(
      Uri.parse(baseUrl + '/patient/${patientId}.json'),
      body: json.encode(body),
    );
    print('2ffffffffffffffffffff');

    if (response.statusCode == 200) {
      return Future.value(unit);
    } else {
      throw ServerException();
    }
  }

  @override
  Future<Unit> deletePatient(String patientId) async {
    print('1ffffffffffffffffffff');
    final response = await client.delete(
      Uri.parse(baseUrl + '/patient/${patientId}.json'),
    );
    print('2ffffffffffffffffffff');

    if (response.statusCode == 200) {
      return Future.value(unit);
    } else {
      throw ServerException();
    }
  }
}
