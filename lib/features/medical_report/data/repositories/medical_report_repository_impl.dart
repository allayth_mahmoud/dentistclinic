// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dartz/dartz.dart';
import 'package:dentist_clinic/core/error/exception.dart';
import 'package:dentist_clinic/core/error/failures.dart';
import 'package:dentist_clinic/core/network/network_info.dart';
import 'package:dentist_clinic/features/medical_report/data/datasources/medical_report_remote_data_source.dart';
import 'package:dentist_clinic/features/medical_report/data/datasources/midical_report_local_data_source.dart';
import 'package:dentist_clinic/features/medical_report/data/models/medical_report_model.dart';
import 'package:dentist_clinic/features/medical_report/domain/entities/medical_report.dart';
import 'package:dentist_clinic/features/medical_report/domain/repositories/medical_report_repositories.dart';

class MedicalReportRepsitoryImpl implements MedicalReportRepository {
  final MedicalReportRemoteDataSource remoteDataSource;
  final MedicalReportLocalDataSource localDataSource;
  final NetworkInfo networkInfo;
  MedicalReportRepsitoryImpl({
    required this.remoteDataSource,
    required this.localDataSource,
    required this.networkInfo,
  });

  @override
  Future<Either<Failure, List<MedicalReport>>> getAllMedicalReportsByPatient(
      String patientId) async {
    if (await networkInfo.isConnected) {
      try {
        final medicalReports =
            await remoteDataSource.getAllMedicalReportsByPatient(patientId);
        localDataSource.cacheMedicalReports(medicalReports, patientId);
        return Right(medicalReports);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      try {
        final cachedMedicalReports =
            await localDataSource.getCachedMedicalReportsByPatient(patientId);
        return Right(cachedMedicalReports);
      } on OfflineException {
        return Left(OfflineFailure());
      }
    }
  }

  @override
  Future<Either<Failure, Unit>> addMedicalReport(
      MedicalReport medicalReport) async {
    print('add medical repo');
    final medicalReportModel = MedicalReportModel(
        id: medicalReport.id,
        patientId: medicalReport.patientId,
        bloodGroup: medicalReport.bloodGroup,
        age: medicalReport.age,
        gender: medicalReport.gender,
        allergies: medicalReport.allergies,
        bloodDisorder: medicalReport.bloodDisorder,
        diabetes: medicalReport.diabetes,
        medications: medicalReport.medications);
    return await _getMessage(() {
      return remoteDataSource.addMedicalReport(medicalReportModel);
    });
  }

  @override
  Future<Either<Failure, Unit>> deleteMedicalReport(
      String medicalReportId) async {
    return await _getMessage(() {
      return remoteDataSource.deleteMedicalReport(medicalReportId);
    });
  }

  @override
  Future<Either<Failure, Unit>> updateMedicalReport(
      MedicalReport medicalReport) async {
    print('update med repo');

    final medicalReportModel = MedicalReportModel(
        id: medicalReport.id,
        patientId: medicalReport.patientId,
        bloodGroup: medicalReport.bloodGroup,
        age: medicalReport.age,
        gender: medicalReport.gender,
        allergies: medicalReport.allergies,
        bloodDisorder: medicalReport.bloodDisorder,
        diabetes: medicalReport.diabetes,
        medications: medicalReport.medications);

    return await _getMessage(() {
      return remoteDataSource.updateMedicalReport(medicalReportModel);
    });
  }

  Future<Either<Failure, Unit>> _getMessage(
      Future<Unit> Function() deleteOrUpdateOrAddPatient) async {
    if (await networkInfo.isConnected) {
      try {
        await deleteOrUpdateOrAddPatient();
        return Right(unit);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(OfflineFailure());
    }
  }
}
