import '../../domain/entities/medical_report.dart';

class MedicalReportModel extends MedicalReport{
  final String? id;
  final String? patientId;

  final String bloodGroup;
  final int age;
  final String gender;

  final String? allergies;
  final String? bloodDisorder;
  final String? diabetes;
  final String? medications;

  MedicalReportModel({
    required this.id,
    required this.patientId,
    required this.bloodGroup,
    required this.age,
    required this.gender,
    required this.allergies,
    required this.bloodDisorder,
    required this.diabetes,
    required this.medications,
  }) : super(id: '', patientId: '', bloodGroup: '', age: 0, gender: '', allergies: '', bloodDisorder: '', diabetes: '', medications: '');

  factory MedicalReportModel.fromMap(Map<String, dynamic> json) {
    return MedicalReportModel(
        id: json['id'],
        patientId: json['patient_id'],
        bloodGroup: json['blood_group'],
        age: json['age'],
        gender: json['gender'],
        allergies: json['alergies'],
        bloodDisorder: json['blood_disorder'],
        diabetes: json['diabetes'],
        medications: json['medications']);
  }

   Map<String, dynamic> toJson() {
    return {
      "id": id,
      "patient_id": patientId,
      "blood_group": bloodGroup,
      "age": age,
      "gender": gender,
      "alergies": allergies, 
      "blood_disorder": bloodDisorder,
      "diabetes": diabetes,
      "medications": medications, 
    };
  }
}
