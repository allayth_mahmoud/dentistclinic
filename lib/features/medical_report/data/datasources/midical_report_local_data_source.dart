// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:dentist_clinic/features/medical_report/data/models/medical_report_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../core/error/exception.dart';

const cached_medical_reports = 'CACHED_MEDICAL_REPORTS';

abstract class MedicalReportLocalDataSource {
  Future<List<MedicalReportModel>> getCachedMedicalReportsByPatient(
      String patientId);
  Future<Unit> cacheMedicalReports(
      List<MedicalReportModel> medicalReportModel, String patientId);
}

class MedicalReportLocalDataSourceImpl implements MedicalReportLocalDataSource {
  final SharedPreferences sharedPreferences;
  MedicalReportLocalDataSourceImpl({
    required this.sharedPreferences,
  });
  @override
  Future<Unit> cacheMedicalReports(
      List<MedicalReportModel> medicalReportModel, String patientId) async {
    final medicalReportModelToJson = medicalReportModel
        .map<Map>((patientModel) => patientModel.toJson())
        .toList();

    await sharedPreferences.setString(cached_medical_reports + '$patientId',
        json.encode(medicalReportModelToJson));
    return Future.value(unit);
  }

  @override
  Future<List<MedicalReportModel>> getCachedMedicalReportsByPatient(
      String patientId) {
    final jsonString =
        sharedPreferences.getString(cached_medical_reports + '$patientId');
    if (jsonString != null) {
      List decodedJsonData = json.decode(jsonString);
      List<MedicalReportModel> jsonToMedicalReportModel = decodedJsonData
          .map((jsonMecicalModel) =>
              MedicalReportModel.fromMap(jsonMecicalModel))
              .where((element) => element.patientId == patientId)
          .toList();
      ;
      return Future.value(jsonToMedicalReportModel);
    } else {
      throw EmptyCacheException();
    }
  }
}
