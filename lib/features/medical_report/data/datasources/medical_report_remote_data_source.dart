import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:http/http.dart' as http;

import '../../../../core/error/exception.dart';
import '../../../../core/utils/constatnts.dart';
import '../../../../core/utils/my_functions.dart';
import '../models/medical_report_model.dart';

abstract class MedicalReportRemoteDataSource {
  Future<List<MedicalReportModel>> getAllMedicalReportsByPatient(
      String patientId);
  Future<Unit> deleteMedicalReport(String medicalReportId);
  Future<Unit> updateMedicalReport(MedicalReportModel medicalReport);
  Future<Unit> addMedicalReport(MedicalReportModel medicalReport);
}

final headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
};

class MedicalReportRemoteDataSouceImpl
    implements MedicalReportRemoteDataSource {
  final http.Client client;

  MedicalReportRemoteDataSouceImpl({required this.client});

  @override
  Future<List<MedicalReportModel>> getAllMedicalReportsByPatient(
      String patientId) async {
    print('mmmmmmmmmmmmmmmmmmmmmgetmmmmmmmmmmmmmmmmmmmmmmm');

    final response = await client.get(
        Uri.parse(
            'https://dentistclinic-fcd69-default-rtdb.firebaseio.com/medical_report.json'),
        headers: headers);
    // if (json.decode(response.body) != Map) return [];
    var responseBody = transform(json.decode(response.body));
    print(responseBody);
    if (response.statusCode == 200) {
      final List medicalReportJson = responseBody;
      final List<MedicalReportModel> medicalReportsModel = medicalReportJson
          .map((medicalReportJson) =>
              MedicalReportModel.fromMap(medicalReportJson))
          .where((element) => element.patientId == patientId)
          .toList();

      print(medicalReportsModel);
      return medicalReportsModel;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<Unit> addMedicalReport(MedicalReportModel medicalReportModel) async {
    final body = {
      "patient_id": medicalReportModel.patientId,
      "blood_group": medicalReportModel.bloodGroup,
      "age": medicalReportModel.age,
      "gender": medicalReportModel.gender,
      "alergies": medicalReportModel.allergies,
      "blood_disorder": medicalReportModel.bloodDisorder,
      "diabetes": medicalReportModel.diabetes,
      "medications": medicalReportModel.medications,
    };
    print('fffffffffffff');

    final response = await client.post(
        Uri.parse(baseUrl + '/medical_report.json'),
        body: json.encode(body));
    print('2fffffffffffff');

    if (response.statusCode == 200) {
      return Future.value(unit);
    } else {
      throw ServerException();
    }
  }

  @override
  Future<Unit> updateMedicalReport(
      MedicalReportModel medicalReportModel) async {
    final body = {
      //"id": medicalReportModel.id,
      "patient_id": medicalReportModel.patientId,
      "blood_group": medicalReportModel.bloodGroup,
      "age": medicalReportModel.age,
      "gender": medicalReportModel.gender,
      "alergies": medicalReportModel.allergies,
      "blood_disorder": medicalReportModel.bloodDisorder,
      "diabetes": medicalReportModel.diabetes,
      "medications": medicalReportModel.medications,
    };
    print('1ffffffffffffffffffffff');
    final response = await client.put(
        Uri.parse(baseUrl + '/medical_report/${medicalReportModel.id}.json'),
        body: json.encode(body));
    print('2ffffffffffffffffffffff');

    if (response.statusCode == 200) {
      return Future.value(unit);
    } else {
      throw ServerException();
    }
  }

  @override
  Future<Unit> deleteMedicalReport(String medicalReportId) async {
    final response =
        await client.delete(Uri.parse(baseUrl + '/medical_report.json'));

    if (response.statusCode == 201) {
      return Future.value(unit);
    } else {
      throw ServerException();
    }
  }
}
