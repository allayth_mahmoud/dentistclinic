import 'package:equatable/equatable.dart';

class MedicalReport extends Equatable {
  final String? id;
  final String? patientId;

  final String bloodGroup;
  final int age;
  final String gender;

  final String? allergies;
  final String? bloodDisorder;
  final String? diabetes;
  final String? medications;

  MedicalReport({
    required this.id,
    required this.patientId,
    required this.bloodGroup,
    required this.age,
    required this.gender,
    required this.allergies,
    required this.bloodDisorder,
    required this.diabetes,
    required this.medications,
  });

  @override
  List<Object?> get props => [
        id,
        patientId,
        bloodDisorder,
        age,
        gender,
        allergies,
        bloodGroup,
        diabetes,
        medications
      ];
}
