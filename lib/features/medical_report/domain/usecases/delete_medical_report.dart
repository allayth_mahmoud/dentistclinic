import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../repositories/medical_report_repositories.dart';

class DeleteMedicalReportUseCase {
  final MedicalReportRepository repository;

  DeleteMedicalReportUseCase({required this.repository});
  Future<Either<Failure, Unit>> call(String medicalReportId) async {
    return await repository.deleteMedicalReport(medicalReportId);
  }
}
