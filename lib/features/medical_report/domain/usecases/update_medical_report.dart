import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../entities/medical_report.dart';
import '../repositories/medical_report_repositories.dart';

class UpdateMedicalReportUseCase {
  final MedicalReportRepository repository;

  UpdateMedicalReportUseCase({required this.repository});
  Future<Either<Failure, Unit>> call(MedicalReport medicalReport) async {
        print('update med usecas');

    return await repository.updateMedicalReport(medicalReport);
  }
}
