import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../entities/medical_report.dart';
import '../repositories/medical_report_repositories.dart';

class AddMedicalReportUseCase {
  final MedicalReportRepository repository;

  AddMedicalReportUseCase({required this.repository});
  Future<Either<Failure, Unit>> call(MedicalReport medicalReport) async {
    print('add medical usecase');
    return await repository.addMedicalReport(medicalReport);
  }
}
