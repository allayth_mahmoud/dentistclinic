import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../entities/medical_report.dart';
import '../repositories/medical_report_repositories.dart';

class GetAllMedicalReportByPatientUseCase {
  final MedicalReportRepository repository;

  GetAllMedicalReportByPatientUseCase({required this.repository});
  Future<Either<Failure, List<MedicalReport>>> call(String patientId) async {
    return await repository.getAllMedicalReportsByPatient(patientId);
  }
}
