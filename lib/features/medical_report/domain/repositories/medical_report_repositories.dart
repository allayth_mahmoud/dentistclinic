

import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../entities/medical_report.dart';
 
abstract class MedicalReportRepository {
  Future<Either<Failure, List<MedicalReport>>> getAllMedicalReportsByPatient(String patientId);
  Future<Either<Failure, Unit>> deleteMedicalReport(String medicalReportId);
  Future<Either<Failure, Unit>> updateMedicalReport(MedicalReport medicalReport);
  Future<Either<Failure, Unit>> addMedicalReport(MedicalReport medicalReport);
}