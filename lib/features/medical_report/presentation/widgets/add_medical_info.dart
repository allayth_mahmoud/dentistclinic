import 'package:flutter/material.dart';

import '../../domain/entities/medical_report.dart';

class AddMedicalInfo extends StatefulWidget {
  MedicalReport editedMedicalReport;
  GlobalKey<FormState> medicalReportKey;

  AddMedicalInfo({
    super.key,
    required this.editedMedicalReport,
    required this.medicalReportKey,
  });

  @override
  State<AddMedicalInfo> createState() => AddMedicalInfoState();
}

class AddMedicalInfoState extends State<AddMedicalInfo> {
  String? _bloodGroup;

  void _bloodGroupOnChanged(String? value) {
    setState(() {
      _bloodGroup = value;
    });
  }

  String? _gender;

  void _genderOnChanged(String? value) {
    setState(() {
      _gender = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Form(
        key: widget.medicalReportKey,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(10, 25, 10, 10),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                    padding: const EdgeInsets.only(bottom: 25),
                    child: DropdownButtonFormField(
                      hint: const Text('Blood Group'),
                      autofocus: true,
                      disabledHint: const Text('Blood Group'),
                      menuMaxHeight: 150,
                      onSaved: (value) {
                        print('onSaved');

                        widget.editedMedicalReport = MedicalReport(
                          id: widget.editedMedicalReport.id,
                          patientId: widget.editedMedicalReport.patientId,
                          bloodGroup: value.toString(),
                          age: widget.editedMedicalReport.age,
                          gender: widget.editedMedicalReport.gender,
                          allergies: widget.editedMedicalReport.allergies,
                          bloodDisorder:
                              widget.editedMedicalReport.bloodDisorder,
                          diabetes: widget.editedMedicalReport.diabetes,
                          medications: widget.editedMedicalReport.medications,
                        );
                      },
                      // Drop down list
                      items: const [
                        DropdownMenuItem(
                          value: 'A+',
                          child: Text('A+'),
                        ),
                        DropdownMenuItem(
                          value: 'A-',
                          child: Text('A-'),
                        ),
                        DropdownMenuItem(
                          value: 'B+',
                          child: Text('B+'),
                        ),
                        DropdownMenuItem(
                          value: 'B-',
                          child: Text('B-'),
                        ),
                        DropdownMenuItem(
                          value: 'AB+',
                          child: Text('AB+'),
                        ),
                        DropdownMenuItem(
                          value: 'AB-',
                          child: Text('AB-'),
                        ),
                        DropdownMenuItem(
                          value: 'O+',
                          child: Text('O+'),
                        ),
                        DropdownMenuItem(
                          value: 'O-',
                          child: Text('O-'),
                        ),
                      ],
                      value: _bloodGroup,
                      onChanged: _bloodGroupOnChanged,
                      validator: (value) {
                        if (value == null) {
                          return 'Please choose a blood group';
                        }
                        return null;
                      },
                    )),
                Padding(
                  padding: const EdgeInsets.only(bottom: 25),
                  child: TextFormField(
                    decoration:
                        const InputDecoration(labelText: 'Blood Disorder'),
                    textInputAction: TextInputAction.next,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please enter a value';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      print('onSaved');

                      widget.editedMedicalReport = MedicalReport(
                        id: widget.editedMedicalReport.id,
                        patientId: widget.editedMedicalReport.patientId,
                        bloodGroup: widget.editedMedicalReport.bloodGroup,
                        age: widget.editedMedicalReport.age,
                        gender: widget.editedMedicalReport.gender,
                        allergies: widget.editedMedicalReport.allergies,
                        bloodDisorder: value.toString(),
                        diabetes: widget.editedMedicalReport.diabetes,
                        medications: widget.editedMedicalReport.medications,
                      );
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 25),
                  child: TextFormField(
                    decoration: const InputDecoration(labelText: 'Allergies'),
                    textInputAction: TextInputAction.next,
                   validator: (value) {
                              if (value!.isEmpty) {
                                return 'Please enter a value';
                              }
                              return null;
                            }, 
                    onSaved: (value) {
                      print('onSaved');
                      widget.editedMedicalReport = MedicalReport(
                        id: widget.editedMedicalReport.id,
                        patientId: widget.editedMedicalReport.patientId,
                        bloodGroup: widget.editedMedicalReport.bloodGroup,
                        age: widget.editedMedicalReport.age,
                        gender: widget.editedMedicalReport.gender,
                        allergies: value.toString(),
                        bloodDisorder: widget.editedMedicalReport.bloodDisorder,
                        diabetes: widget.editedMedicalReport.diabetes,
                        medications: widget.editedMedicalReport.medications,
                      );
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 25),
                  child: TextFormField(
                    decoration: const InputDecoration(labelText: 'Diabetes'),
                    textInputAction: TextInputAction.next,
                     validator: (value) {
                              if (value!.isEmpty) {
                                return 'Please enter a value';
                              }
                              return null;
                            }, 
                    onSaved: (value) {
                      widget.editedMedicalReport = MedicalReport(
                        id: widget.editedMedicalReport.id,
                        patientId: widget.editedMedicalReport.patientId,
                        bloodGroup: widget.editedMedicalReport.bloodGroup,
                        age: widget.editedMedicalReport.age,
                        gender: widget.editedMedicalReport.gender,
                        allergies: widget.editedMedicalReport.allergies,
                        bloodDisorder: widget.editedMedicalReport.bloodDisorder,
                        diabetes: value.toString(),
                        medications: widget.editedMedicalReport.medications,
                      );
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 25),
                  child: TextFormField(
                    decoration: const InputDecoration(labelText: 'Age'),
                    textInputAction: TextInputAction.next,
                    validator: (value) {
                      if (value!.isEmpty) return 'Please enter a value';
                      return null;
                    },
                    onSaved: (value) {
                      widget.editedMedicalReport = MedicalReport(
                        id: widget.editedMedicalReport.id,
                        patientId: widget.editedMedicalReport.patientId,
                        bloodGroup: widget.editedMedicalReport.bloodGroup,
                        age: int.parse(value!),
                        gender: widget.editedMedicalReport.gender,
                        allergies: widget.editedMedicalReport.allergies,
                        bloodDisorder: widget.editedMedicalReport.bloodDisorder,
                        diabetes: widget.editedMedicalReport.diabetes,
                        medications: widget.editedMedicalReport.medications,
                      );
                    },
                    keyboardType: TextInputType.number,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 25),
                  child: DropdownButtonFormField(
                    hint: const Text('Gender'),
                    items: const [
                      DropdownMenuItem(
                        value: 'male',
                        child: Text('Male'),
                      ),
                      DropdownMenuItem(
                        value: 'female',
                        child: Text('Female'),
                      ),
                    ],
                    value: _gender,
                    onChanged: _genderOnChanged,
                    onSaved: (value) {
                      widget.editedMedicalReport = MedicalReport(
                        id: widget.editedMedicalReport.id,
                        patientId: widget.editedMedicalReport.patientId,
                        bloodGroup: widget.editedMedicalReport.bloodGroup,
                        age: widget.editedMedicalReport.age,
                        gender: value.toString(),
                        allergies: widget.editedMedicalReport.allergies,
                        bloodDisorder: widget.editedMedicalReport.bloodDisorder,
                        diabetes: widget.editedMedicalReport.diabetes,
                        medications: widget.editedMedicalReport.medications,
                      );
                    },
                    validator: (value) {
                      if (value == null) {
                        return 'Please choose gender';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 25),
                  child: TextFormField(
                    decoration: const InputDecoration(labelText: 'Medications'),
                    textInputAction: TextInputAction.next,
                    onSaved: (value) {
                      widget.editedMedicalReport = MedicalReport(
                        id: widget.editedMedicalReport.id,
                        patientId: widget.editedMedicalReport.patientId,
                        bloodGroup: widget.editedMedicalReport.bloodGroup,
                        age: widget.editedMedicalReport.age,
                        gender: widget.editedMedicalReport.gender,
                        allergies: widget.editedMedicalReport.allergies,
                        bloodDisorder: widget.editedMedicalReport.bloodDisorder,
                        diabetes: widget.editedMedicalReport.diabetes,
                        medications: value.toString(),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
