import '../bloc/medical_reports/medical_reports_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/widgets/shimmer_widgets.dart';
import '../../../../core/utils/constatnts.dart';
import '../../../../core/widgets/loading_widget copy.dart';
import '../screens/add_medical_info_screen.dart';
import '../../../../core/widgets/message_display_widget.dart';
import 'medical_info_widget.dart';

class MedicalInfo extends StatefulWidget {
  static const routeName = '/personal-info-screen';
  final patient_id;

  const MedicalInfo({super.key, required this.patient_id});

  @override
  State<MedicalInfo> createState() => _MedicalInfoState();
}

class _MedicalInfoState extends State<MedicalInfo> {
  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.sizeOf(context);
    return Scaffold(
        body: SizedBox(
      height: media.height * 0.83,
      child: BlocBuilder<MedicalReportsBloc, MedicalReportsState>(
        builder: (context, state) {
          if (state is LoadingMedicalReportState) {
            return  ListView.builder(
                        itemBuilder: (context, int index) {
                          return ShimmerWidgets.patientCardShimmer();
                        },
                        itemCount: 20,
                      );
          } else if (state is LoadedMedicalReportState) {
            return state.medicalReport.isEmpty
                ? Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'There is no medical report... please add one',
                            style: TextStyle(
                                color: myMainColor,
                                fontWeight: FontWeight.bold,
                                fontSize: 19),
                          ),
                          SizedBox(
                            height: media.height * 0.05,
                          ),
                          ElevatedButton(
                            onPressed: () {
                              addMedicalInfoDialog(context, widget.patient_id, false);
                            },
                            style: ElevatedButton.styleFrom(
                                backgroundColor: mySecondColor,
                                shape: const StadiumBorder()),
                            child: const Text("Add Medical Info"),
                          ),
                        ]),
                  ) 
                : MedicalInfoWidget(
                    media: media,
                    medical_report: state.medicalReport,
                  );
          } else if (state is ErrorMessageMedicalReportState) {
            return MessageDisplayWidget(message: state.message);
          }
          return LoadingWidget();
        },
      ),
    ));
  }
}

addMedicalInfoDialog(BuildContext context, String patient_id, bool edit) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Container(
          // height: 200.0,
          // width: 200.0,
          padding: EdgeInsets.all(20.0),
          child: AddMedicalInfoScreen(
             
            edit: edit,
            patinet_id: patient_id,
          ),
        ),
      );
    },
  );
}
