// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

import '../../../../core/utils/constatnts.dart';
import '../../../patients/presentation/widgets/info_card.dart';
import '../screens/add_medical_info_screen.dart';

class MedicalInfoWidget extends StatelessWidget {
  final media;
  final medical_report;
  const MedicalInfoWidget({
    Key? key,
    required this.media,
    this.medical_report,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print(medical_report);
    return ListView(
      children: [
        SizedBox(
          height: media.height * 0.05,
          child: Row(
            // mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Icon(
                Icons.edit,
                color: mySecondColor,
              ),
              TextButton( 
                onPressed: () { 
                  updateMedicalInfoDialog(
                      context, medical_report[0].patientId, true ,medical_report[0].id);
                },
                child: const Text(
                  "Edit Info",
                  textAlign: TextAlign.start,
                  style: TextStyle(color: mySecondColor),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: media.height * 0.12,
          child: InfoCard(
              icon: const Icon(
                Icons.person,
                color: mySecondColor,
              ),
              title: 'Age: ',
              value: medical_report[0].age.toString()),
        ),
        SizedBox(
          height: media.height * 0.12,
          child: InfoCard(
              icon: const Icon(
                Icons.person,
                color: mySecondColor,
              ),
              title: 'Gender: ',
              value: medical_report[0].gender.toString()),
        ),
        SizedBox(
          height: media.height * 0.12,
          child: InfoCard(
              icon: const Icon(
                Icons.bloodtype_rounded,
                color: mySecondColor,
              ),
              title: 'Blood Type: ',
              value: medical_report[0].bloodGroup),
        ),
        SizedBox(
          height: media.height * 0.12,
          child: InfoCard(
              icon: const Icon(
                Icons.medication,
                color: mySecondColor,
              ),
              title: 'Blood Disorder: ',
              value: medical_report[0].bloodDisorder),
        ),
        SizedBox(
          height: media.height * 0.12,
          child: InfoCard(
              icon: const Icon(
                Icons.medication,
                color: mySecondColor,
              ),
              title: 'Alergies: ',
              value: medical_report[0].allergies),
        ),
        SizedBox(
          height: media.height * 0.12,
          child: InfoCard(
              icon: const Icon(
                Icons.medication,
                color: mySecondColor,
              ),
              title: 'Diabeties: ',
              value: medical_report[0].diabetes),
        ),
        SizedBox(
          child: InfoCard(
              icon: const Icon(
                Icons.medication,
                color: mySecondColor,
              ),
              title: 'Diabeties: ',
              value: medical_report[0].medications),
        ),
      ],
    );
  }
}

updateMedicalInfoDialog(BuildContext context, String patient_id, bool edit, medicalId) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Container(
       
          padding: EdgeInsets.all(20.0),
          child: AddMedicalInfoScreen(
            edit: edit,
            patinet_id: patient_id,
            medical_id: medicalId,
          ),
        ),
      );
    },
  );
}
