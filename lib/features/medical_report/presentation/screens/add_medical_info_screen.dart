
import '../bloc/medical_reports/medical_reports_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/utils/constatnts.dart';
import '../../../../core/widgets/loading_widget.dart';
import '../../../../core/widgets/snackbar_message.dart';
import '../../domain/entities/medical_report.dart';
import '../widgets/add_medical_info.dart';

class AddMedicalInfoScreen extends StatefulWidget {
  const AddMedicalInfoScreen({
    Key? key,
    required this.patinet_id,
    required this.edit,
    this.medical_id,
  }) : super(key: key);

  static const routeName = '/add-medical-info-screen';
  final patinet_id;
  final String? medical_id;
  final bool edit;

  @override
  State<AddMedicalInfoScreen> createState() => _AddMedicalInfoScreenState();
}

class _AddMedicalInfoScreenState extends State<AddMedicalInfoScreen> {
  final personalInfoKey = GlobalKey<FormState>();
  final medicalReportKey = GlobalKey<FormState>();

  GlobalKey<AddMedicalInfoState> medicalStepKey =
      GlobalKey<AddMedicalInfoState>();

  MedicalReport editedMedicalReport = MedicalReport(
      id: '',
      patientId: '',
      bloodGroup: '',
      age: 0,
      gender: '',
      allergies: '',
      bloodDisorder: '',
      diabetes: '',
      medications: '');

  String image = '';

  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.sizeOf(context);
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          centerTitle: true,
          title: Text(widget.edit ? 'Edit Medical Info' : 'Add Medical Info'),
          backgroundColor: myMainColor,
          toolbarHeight: 70,
        ),
        body: BlocConsumer<MedicalReportsBloc, MedicalReportsState>(
          listener: (context, state) {
            if (state is DoneMessageMedicalReportState) {
              SnackBarMessage().showSuccessSnackBar(
                  message: state.message, context: context);
              BlocProvider.of<MedicalReportsBloc>(context).add(
                  GetMedicalReportByPatientEvent(patientId: widget.patinet_id));

              Navigator.of(context).pop();

             
            } else if (state is ErrorMessageMedicalReportState) {
              SnackBarMessage()
                  .showErrorSnackBar(message: state.message, context: context);
              Navigator.of(context).pop();
                }
          },
          builder: (context, state) {
            if (state is LoadingMedicalReportState) {
              return LoadingWidget();
            }

            return Column(
              children: [
                SizedBox(
                  height: media.height * 0.68,
                  child: AddMedicalInfo(
                    key: medicalStepKey,
                    editedMedicalReport: editedMedicalReport,
                    medicalReportKey: medicalReportKey,
                  ),
                ),
                SizedBox(
                  height: media.height * 0.08,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: const Text(
                          "Cancel",
                          style: TextStyle(color: mySecondColor),
                        ),
                      ),
                      ElevatedButton(
                        onPressed: () {
                          addOrUpdateMedicalInfo(
                              medicalStepKey,
                              editedMedicalReport,
                              context,
                              widget.patinet_id,
                              widget.edit,
                              widget.medical_id);
                        },
                        style: ElevatedButton.styleFrom(
                            backgroundColor: mySecondColor,
                            shape: const StadiumBorder()),
                        child: Text(widget.edit ? "Update" : "Add"),
                      ),
                    ],
                  ),
                )
              ],
            );
          },
        ));
  }
}

void addOrUpdateMedicalInfo(medicalStepKey, MedicalReport editedMedicalReport,
    context, patientId, edit, medicalId) {
  // setting new values

  medicalStepKey.currentState?.widget.medicalReportKey.currentState?.save();

  editedMedicalReport = medicalStepKey.currentState!.widget.editedMedicalReport;

  print(editedMedicalReport);
  MedicalReport editedMedicalReportFinal = MedicalReport(
      patientId: patientId.toString(),
      bloodGroup: editedMedicalReport.bloodGroup.toString(),
      age: editedMedicalReport.age,
      gender: editedMedicalReport.gender.toString(),
      allergies: editedMedicalReport.allergies.toString(),
      bloodDisorder: editedMedicalReport.bloodDisorder.toString(),
      diabetes: editedMedicalReport.diabetes.toString(),
      medications: editedMedicalReport.medications.toString(),
      id: edit ? medicalId : '');

  print(editedMedicalReportFinal);

  edit
      ? BlocProvider.of<MedicalReportsBloc>(context).add(
          UpdateMedicalReportEvent(medicalReport: editedMedicalReportFinal))
      : BlocProvider.of<MedicalReportsBloc>(context)
          .add(AddMedicalReportEvent(medicalReport: editedMedicalReportFinal));
}
