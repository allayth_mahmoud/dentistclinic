// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'medical_reports_bloc.dart';

abstract class MedicalReportsState extends Equatable {
  const MedicalReportsState();

  @override
  List<Object> get props => [];
}

class MedicalReportsInitial extends MedicalReportsState {}

class LoadingMedicalReportState extends MedicalReportsState {}

class LoadedMedicalReportState extends MedicalReportsState {
  final List<MedicalReport> medicalReport;
  LoadedMedicalReportState({
    required this.medicalReport,
  });


    @override
  List<Object> get props => [medicalReport];
}


class DoneMessageMedicalReportState extends MedicalReportsState {
  final String message;
  DoneMessageMedicalReportState({
    required this.message,
  });


    @override
  List<Object> get props => [message];
}

class ErrorMessageMedicalReportState extends MedicalReportsState {
  final String message;
  ErrorMessageMedicalReportState({
    required this.message,
  });
    @override
  List<Object> get props => [message];
}
