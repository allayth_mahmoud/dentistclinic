import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../../../../core/error/failures.dart';
import '../../../../../core/strings/failures.dart';
import '../../../../../core/strings/messages.dart';
import '../../../domain/entities/medical_report.dart';
import '../../../domain/usecases/add_medical_report.dart';
import '../../../domain/usecases/delete_medical_report.dart';
import '../../../domain/usecases/get_all_medical_report_by_patient.dart';
import '../../../domain/usecases/update_medical_report.dart';

part 'medical_reports_event.dart';
part 'medical_reports_state.dart';

class MedicalReportsBloc
    extends Bloc<MedicalReportsEvent, MedicalReportsState> {
  final GetAllMedicalReportByPatientUseCase getAllMedicalReportByPatientUseCase;
  final AddMedicalReportUseCase addMedicalReportUseCase;
  final UpdateMedicalReportUseCase updateMedicalReportUseCase;
  final DeleteMedicalReportUseCase deleteMedicalReportUseCase;
  MedicalReportsBloc(
      this.getAllMedicalReportByPatientUseCase,
      this.addMedicalReportUseCase,
      this.updateMedicalReportUseCase,
      this.deleteMedicalReportUseCase)
      : super(MedicalReportsInitial()) {
    on<MedicalReportsEvent>((event, emit) async {

       if (event is GetMedicalReportByPatientEvent) {
        emit(LoadingMedicalReportState());

        final failureOrMedical = await getAllMedicalReportByPatientUseCase(event.patientId);
        emit(_mapFailureOrPatientsToState(failureOrMedical));
      } else  if (event is AddMedicalReportEvent) {
        emit(LoadingMedicalReportState());

        final failureOrAddPatient = await addMedicalReportUseCase(event.medicalReport);
        emit(_eitherDoneMessageOrErrorState(
            failureOrAddPatient, ADD_SUCCESS_MESSAGE));
      } else if (event is UpdateMedicalReportEvent) {
        emit(LoadingMedicalReportState());

        final failureOrDoneMessage = await updateMedicalReportUseCase(event.medicalReport);

        emit(
          _eitherDoneMessageOrErrorState(
              failureOrDoneMessage, UPDATE_SUCCESS_MESSAGE),
        );
      } else if (event is DeleteMedicalReportEvent) {
        emit(LoadingMedicalReportState());

        final failureOrDoneMessage =
            await deleteMedicalReportUseCase(event.patientId);

        emit(
          _eitherDoneMessageOrErrorState(
              failureOrDoneMessage, DELETE_SUCCESS_MESSAGE),
        );
      }


    });
  }

   MedicalReportsState _eitherDoneMessageOrErrorState(
      Either<Failure, Unit> either, String message) {
    return either.fold(
      (failure) => ErrorMessageMedicalReportState(
        message: _mapFailureToMessage(failure),
      ),
      (_) => DoneMessageMedicalReportState(message: message),
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case OfflineFailure:
        return OFFLINE_FAILURE_MESSAGE;
      default:
        return "Unexpected Error , Please try again later .";
    }
  }

   MedicalReportsState _mapFailureOrPatientsToState(
      Either<Failure, List<MedicalReport>> either) {
    return either.fold(
      (failure) => ErrorMessageMedicalReportState(message: _mapFailureToMessage(failure)),
      (medicalReport) => LoadedMedicalReportState(
        medicalReport: medicalReport,
      ),
    );
  }

   
}
