// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'medical_reports_bloc.dart';

abstract class MedicalReportsEvent extends Equatable {
  const MedicalReportsEvent();

  @override
  List<Object> get props => [];
}

class GetMedicalReportByPatientEvent extends MedicalReportsEvent {
  final String patientId;
  GetMedicalReportByPatientEvent({
    required this.patientId,
  });
  @override
  List<Object> get props => [patientId];
}

class AddMedicalReportEvent extends MedicalReportsEvent {
  final MedicalReport medicalReport;
  AddMedicalReportEvent({
    required this.medicalReport,
  });
   @override
  List<Object> get props => [medicalReport];
}

class UpdateMedicalReportEvent extends MedicalReportsEvent {
  final MedicalReport medicalReport;
 UpdateMedicalReportEvent({
    required this.medicalReport,
  });
   @override
  List<Object> get props => [medicalReport];
}

class DeleteMedicalReportEvent extends MedicalReportsEvent {
  final String patientId;
  DeleteMedicalReportEvent({
    required this.patientId,
  });
   @override
  List<Object> get props => [patientId];
}
