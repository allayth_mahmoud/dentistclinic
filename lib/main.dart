import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'features/medical_report/presentation/bloc/medical_reports/medical_reports_bloc.dart';
import 'features/patients/presentation/bloc/add_delete_update_patient/add_delete_update_patient_bloc.dart';
import 'features/patients/presentation/bloc/patients/patient_bloc.dart';
import 'features/patients/presentation/screens/add_patient_screen.dart';
import 'features/patients/presentation/screens/patient_info_screen.dart';
import 'features/patients/presentation/screens/patients_list_screen.dart';
import 'features/sessions/presentation/bloc/add_delete_update_session/add_delete_udate_session_bloc.dart';
import 'features/sessions/presentation/bloc/sessions/sessions_bloc.dart';
import 'features/sessions/presentation/screens/schedule_screen.dart';
import 'home_screen.dart';
import 'injection_container.dart' as di;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => di.sl<PatientBloc>()),
        BlocProvider(create: (_) => di.sl<SessionsBloc>()),
        BlocProvider(create: (_) => di.sl<AddDeleteUpdatePatientBloc>()),

        BlocProvider(create: (_) => di.sl<AddDeleteUdateSessionBloc>()),
        BlocProvider(create: (_) => di.sl<MedicalReportsBloc>()),
        //  BlocProvider(
        //       create: (_) => di.sl<PostsBloc>()..add(GetAllPostsEvent())),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        //   theme: appTheme
        // ThemeData(
        //   primarySwatch: Colors.blue,
        //   fontFamily: 'Lato',
        // ),

        home: const HomeScreen(),

        routes: {
          HomeScreen.routeName: (context) => const HomeScreen(),
          PationtsListScreen.routeName: (context) => PationtsListScreen(),
          AddPatientScreen.routeName: (context) =>
              AddPatientScreen(edit: false),
          PatientInfoScreen.routeName: (context) => PatientInfoScreen(),
          ScheduleScreen.routeName: (context) => ScheduleScreen(),

          // AllPatientsScreen.routeName: (context) => AllPatientsScreen(),
          // MedicalInfoScreen.routeName: (context) => MedicalInfoScreen(),
          // PatinetSessionsScreen.routeName: (context) =>
          //     const PatinetSessionsScreen(),
          // PersonalInfoScreen.routeName: (context) => PersonalInfoScreen(),
          // StepperScreen.routeName: (context) => const StepperScreen(),
          // ScheduleScreen.routeName: (context) => const ScheduleScreen(),
          // SettingsScreen.routeName: (context) => const SettingsScreen(),
          // AuthScreen.routeName: (context) => const AuthScreen(),
        },
      ),
    );
  }
}
