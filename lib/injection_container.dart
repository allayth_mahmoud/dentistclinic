import 'features/medical_report/data/datasources/medical_report_remote_data_source.dart';
import 'features/medical_report/data/datasources/midical_report_local_data_source.dart';
import 'features/medical_report/data/repositories/medical_report_repository_impl.dart';
import 'features/medical_report/domain/repositories/medical_report_repositories.dart';
import 'features/medical_report/domain/usecases/add_medical_report.dart';
import 'features/medical_report/domain/usecases/delete_medical_report.dart';
import 'features/medical_report/domain/usecases/get_all_medical_report_by_patient.dart';
import 'features/medical_report/domain/usecases/update_medical_report.dart';
import 'features/medical_report/presentation/bloc/medical_reports/medical_reports_bloc.dart';
import 'features/patients/data/datasources/patient_local_data_source.dart';
import 'features/patients/data/datasources/patient_remote_data_source.dart';
import 'features/patients/data/repositories/patient_repository_impl.dart';
import 'features/patients/domain/repositories/patient_repositories.dart';
import 'features/patients/domain/usecases/add_patient.dart';
import 'features/patients/domain/usecases/delete_pateint.dart';
import 'features/patients/domain/usecases/get_all_patients.dart';
import 'features/patients/domain/usecases/update_patient.dart';
import 'features/patients/presentation/bloc/add_delete_update_patient/add_delete_update_patient_bloc.dart';
import 'features/patients/presentation/bloc/patients/patient_bloc.dart';
import 'features/sessions/data/datasources/session_local_data_source.dart';
import 'features/sessions/data/datasources/session_remote_data_source.dart';
import 'features/sessions/data/repositories/session_repository_impl.dart';
import 'features/sessions/domain/repositories/session_repositories.dart';
import 'features/sessions/domain/usecases/add_session.dart';
import 'features/sessions/domain/usecases/delete_session.dart';
import 'features/sessions/domain/usecases/get_session_by_date.dart';
import 'features/sessions/domain/usecases/get_session_by_patient.dart';
import 'features/sessions/domain/usecases/update_session.dart';
import 'features/sessions/presentation/bloc/add_delete_update_session/add_delete_udate_session_bloc.dart';
import 'features/sessions/presentation/bloc/sessions/sessions_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'core/network/network_info.dart';

final sl = GetIt.instance;

Future<void> init() async {
//! Features - posts

// Bloc

  sl.registerFactory(() => PatientBloc(sl()));
  sl.registerFactory(() => AddDeleteUpdatePatientBloc(
      addPatientUseCase: sl(),
      updatePatientUseCase: sl(),
      deletePatientUseCase: sl()));

  sl.registerFactory(() => SessionsBloc(sl(), sl()));
  sl.registerFactory(() => AddDeleteUdateSessionBloc(
      addSessionUseCase: sl(),
      deleteSessionUseCase: sl(),
      updateSessionUseCase: sl()));

  sl.registerFactory(() => MedicalReportsBloc(sl(), sl(), sl(), sl()));

// Usecases

  sl.registerLazySingleton(() => AddPatientUseCase(sl()));
  sl.registerLazySingleton(() => GetAllPatientUseCase(sl()));
  sl.registerLazySingleton(() => DeletePatientUseCase(sl()));
  sl.registerLazySingleton(() => UpdatePatientUseCase(sl()));

  sl.registerLazySingleton(() => AddSessionUseCase(repository: sl()));
  sl.registerLazySingleton(() => GetSessionByDateUseCase(repository: sl()));
  sl.registerLazySingleton(() => GetSessionByPatientUseCase(repository: sl()));
  sl.registerLazySingleton(() => DeleteSessionUseCase(repository: sl()));
  sl.registerLazySingleton(() => UpdateSessionUseCase(repository: sl()));

  sl.registerLazySingleton(() => AddMedicalReportUseCase(repository: sl()));
  sl.registerLazySingleton(
      () => GetAllMedicalReportByPatientUseCase(repository: sl()));
  sl.registerLazySingleton(() => DeleteMedicalReportUseCase(repository: sl()));
  sl.registerLazySingleton(() => UpdateMedicalReportUseCase(repository: sl()));

// Repository

  sl.registerLazySingleton<PatientRepository>(() => PatientRepositoryImpl(
      remoteDataSource: sl(), localDataSource: sl(), networkInfo: sl()));

  sl.registerLazySingleton<SessionRepository>(
      () => SessionRepositoryImpl(sl(), sl(), sl()));

  sl.registerLazySingleton<MedicalReportRepository>(() =>
      MedicalReportRepsitoryImpl(
          remoteDataSource: sl(), localDataSource: sl(), networkInfo: sl()));

// Datasources

  sl.registerLazySingleton<PatientRemoteDataSource>(
      () => PatientRemoteDataSourceImpl(client: sl()));
  sl.registerLazySingleton<PatientLocalDataSource>(
      () => PatientLocalDataSourceImpl(sharedPreferences: sl()));

  sl.registerLazySingleton<SessionRemoteDataSource>(
      () => SessionRemoteDataSourceImpl(client: sl()));
  sl.registerLazySingleton<SessionLocalDataSource>(
      () => SessionLocalDataSourceImpl(sharedPreferences: sl()));

  sl.registerLazySingleton<MedicalReportRemoteDataSource>(
      () => MedicalReportRemoteDataSouceImpl(client: sl()));
  sl.registerLazySingleton<MedicalReportLocalDataSource>(
      () => MedicalReportLocalDataSourceImpl(sharedPreferences: sl()));

//! Core

  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));

//! External

  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton(() => http.Client());
  sl.registerLazySingleton(() => InternetConnectionChecker());
}
