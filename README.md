# dentist_clinic

"dentist_clinic" is a mobile app built with Flutter that allows dentists and dental staff to manage patient and session information in a secure and organized way. The app is designed using Clean Architecture principles, ensuring that the code is modular, testable, and maintainable.

The app uses BLoC state management to manage the state of the app and handle interactions with Firebase, a cloud-based database. When a new patient is added, the app sends a request to Firebase to store the patient's details, including their name, age, contact information, and medical history. The app then uses the BLoC pattern to manage the state of the patient information and display it in the app.

In addition to adding new patients, "dentist_clinic" also allows dental staff to schedule sessions and appointments for each patient. Staff can view their daily and weekly schedules, and see which patients are due for appointments. The app also allows staff to view the medical history and notes for each patient, ensuring that they have all the information they need at their fingertips.

The app also includes a staff management system, allowing dentists to add and remove staff members as needed. Staff members can be given different levels of access to patient information, ensuring that sensitive data is kept secure.

Overall, "dentist_clinic" is a comprehensive and easy-to-use app for dentists and dental staff. Its use of Flutter and BLoC state management allows for a smooth and seamless user experience, while its integration with Firebase ensures that patient and session information is stored securely in the cloud. Its use of Clean Architecture principles also ensures that the code is well-organized and easy to maintain.
